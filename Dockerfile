# running from root directory
# build with:# docker build -t data-fabric-betting-history-api .
# run with:
# docker run -d -p 80:8181 data-fabric-betting-history-api
#FROM 469538696552.dkr.ecr.eu-west-2.amazonaws.com/base-images/maven-service
FROM 469538696552.dkr.ecr.eu-west-2.amazonaws.com/base-images/maven-service-appdyn
ADD data-fabric-ras-map-api/target/data-fabric-ras-map-api-develop-SNAPSHOT.jar  app.jar
ENV NEW_RELIC_APP_NAME LC-CRE-DF-RASGWY-DEV
