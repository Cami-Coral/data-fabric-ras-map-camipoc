package com.lc.df.service.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"transaction", "code", "description", "severity", "message"}
)
@XmlRootElement(
        name = "error",
        namespace = ""
)
public class Error implements Serializable {
    private static final long serialVersionUID = 54L;
    @XmlElement(
            namespace = "",
            required = true
    )
    protected Error.Transaction transaction;
    @XmlElement(
            namespace = ""
    )
    protected String code;
    @XmlElement(
            namespace = ""
    )
    protected String description;
    @XmlElement(
            namespace = ""
    )
    protected String severity;
    @XmlElement(
            namespace = ""
    )
    protected String message;

    public Error() {
    }

    public Transaction getTransaction() {
        return this.transaction;
    }

    public void setTransaction(Transaction value) {
        this.transaction = value;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String value) {
        this.code = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public String getSeverity() {
        return this.severity;
    }

    public void setSeverity(String value) {
        this.severity = value;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String value) {
        this.message = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
            name = "",
            propOrder = {"value"}
    )
    public static class Transaction implements Serializable {
        private static final long serialVersionUID = 54L;
        @XmlValue
        protected String value;
        @XmlAttribute(
                name = "code"
        )
        protected Integer code;

        public Transaction() {
        }

        public String getValue() {
            return this.value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Integer getCode() {
            return this.code;
        }

        public void setCode(Integer value) {
            this.code = value;
        }
    }
}
