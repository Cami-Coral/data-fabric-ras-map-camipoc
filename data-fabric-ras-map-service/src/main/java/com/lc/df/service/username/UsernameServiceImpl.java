package com.lc.df.service.username;

import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.logging.DataFabricLogger;
import com.playtech.ims.Error;
import com.playtech.ims.GetIdTokenInfoRequest;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import static com.lc.df.service.utils.Constants.DF;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_AUTH_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_CASINO_NAME;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_IDENTITY_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_ID_TOKEN_RFID_PRINTED_TOKEN_CODE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_MESSAGE_ID;
import static com.lc.df.service.utils.Constants.LOG_SELF;
import static com.lc.df.service.utils.Constants.LOG_STATUS_ERROR;
import static com.lc.df.service.utils.Constants.LOG_STATUS_OK;
import static com.lc.df.service.utils.Constants.LOG_STATUS_PREP_REQ;
import static com.lc.df.service.utils.Constants.LOG_TYPE_REQUEST;
import static com.lc.df.service.utils.Constants.LOG_TYPE_RESPONSE;
import static com.lc.df.service.utils.Constants.X_AUTH_TYPE_DEFAULT_VALUE;
import static com.lc.df.service.utils.Constants.X_IDENTITY_TYPE_CARD_NUMBER_VALUE;

@Service
public class UsernameServiceImpl implements UsernameService {

    @Autowired
    DataFabricLogger log;

    @Autowired
    RestTemplate imsSSLRestTemplate;

    XmlMapper xmlMapper = new XmlMapper();

    @Override
    public String getUsernameByCardNumber(UsernameRequestCommand usernameRequestCommand) throws DataFabricException {
        try {
            GetIdTokenInfoResponse response = getUsernameResponseByCardNumber(usernameRequestCommand);
            return xmlMapper.writeValueAsString(response);
        } catch (Exception e) {
            return handleException(usernameRequestCommand, e);
        }
    }

    @Override
    public GetIdTokenInfoResponse getUsernameResponseByCardNumber(UsernameRequestCommand usernameRequestCommand) {
        String appName = usernameRequestCommand.getAppName();

        HttpEntity<GetIdTokenInfoRequest> httpEntity = new HttpEntity<>(buildUsernameRequest(),
                createHeadersForIMS(usernameRequestCommand));

        String timeKey = log.startTimer();
        log.log(appName, Level.INFO, httpEntity, LOG_TYPE_REQUEST, usernameRequestCommand.getHeaders().getXLcCid(),
                usernameRequestCommand.getHeaders().getXClientRequestId(), usernameRequestCommand.getHeaders().getXForwardedFor(),
                null, LOG_SELF, usernameRequestCommand.getUrl(), LOG_STATUS_PREP_REQ);

        GetIdTokenInfoResponse response =imsSSLRestTemplate.postForObject(usernameRequestCommand.getUrl(),
                httpEntity,
                GetIdTokenInfoResponse.class);

        log.log(appName, Level.INFO, response, LOG_TYPE_RESPONSE, usernameRequestCommand.getHeaders().getXLcCid(),
                usernameRequestCommand.getHeaders().getXClientRequestId(), usernameRequestCommand.getHeaders().getXForwardedFor(),
                log.stopTimer(timeKey), usernameRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_OK);

        return response;
    }

    private String handleException(UsernameRequestCommand usernameRequestCommand, Exception e) throws DataFabricException {
        String appName = usernameRequestCommand.getAppName();

        log.log(appName, Level.ERROR,"IMS - Get Id Token Info - ERROR: " + ExceptionUtils.getStackTrace(e),
                LOG_TYPE_RESPONSE, usernameRequestCommand.getHeaders().getXLcCid(),
                usernameRequestCommand.getHeaders().getXClientRequestId(), usernameRequestCommand.getHeaders().getXForwardedFor(),
                null, usernameRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
        Error error = new Error();
        try {
            if (e instanceof HttpStatusCodeException) {
                error.setCode(String.valueOf(HttpStatus.OK));
                error.setDescription(String.format("Username cannot be found for card [%s]. Error: [%s]",
                        usernameRequestCommand.getCardNumber(), e.getMessage()));

                    return xmlMapper.writeValueAsString(error);

            }
            error.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
            error.setDescription(String.format("IMS connectivity issues. Error: [%s]", e.getMessage()));
            return xmlMapper.writeValueAsString(error);
        } catch (JsonProcessingException jpe) {
            throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, jpe.getMessage(), DF);
        }
    }

    private GetIdTokenInfoRequest buildUsernameRequest() {
        return new GetIdTokenInfoRequest();
    }

    private HttpHeaders createHeadersForIMS(UsernameRequestCommand usernameRequestCommand) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(IMS_HEADER_X_MESSAGE_ID, usernameRequestCommand.getHeaders().getXLcCid());
        headers.add(IMS_HEADER_X_CASINO_NAME, usernameRequestCommand.getHeaders().getCasinoName());
        headers.add(IMS_HEADER_X_IDENTITY_TYPE, X_IDENTITY_TYPE_CARD_NUMBER_VALUE);
        headers.add(IMS_HEADER_X_AUTH_TYPE, X_AUTH_TYPE_DEFAULT_VALUE);
        headers.add(IMS_HEADER_X_ID_TOKEN_RFID_PRINTED_TOKEN_CODE, usernameRequestCommand.getCardNumber());
        return headers;
    }
}
