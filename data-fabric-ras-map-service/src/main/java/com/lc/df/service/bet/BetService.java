package com.lc.df.service.bet;

import com.lc.df.ras.betting.request.CancelBetRequestCommand;
import com.lc.df.ras.betting.request.PlaceBetRequestCommand;
import com.lc.df.ras.betting.request.SettleBetRequestCommand;
import com.lc.df.ras.betting.request.UnsettleBetRequestCommand;
import com.lc.df.ras.betting.response.BetPlacementResult;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.error.model.DataFabricException;

public interface BetService {
    BetPlacementResult placeBet(PlaceBetRequestCommand betRequestCommand) throws DataFabricException;
    CallResult cancelBet(CancelBetRequestCommand cancelBetRequestCommand) throws DataFabricException;
    CallResult settleBet(SettleBetRequestCommand settleBetRequestCommand) throws DataFabricException;
    CallResult unsettleBet(UnsettleBetRequestCommand unsettleBetRequestCommand) throws DataFabricException;
}
