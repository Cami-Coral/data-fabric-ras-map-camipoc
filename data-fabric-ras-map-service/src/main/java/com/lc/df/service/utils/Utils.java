package com.lc.df.service.utils;

import ch.qos.logback.classic.Level;
import com.google.common.io.CharStreams;
import com.lc.df.logging.DataFabricLogger;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.text.DecimalFormat;

import static com.lc.df.service.utils.Constants.DF;
import static com.lc.df.service.utils.Constants.ENV_PROD;
import static com.lc.df.service.utils.Constants.GVC_CLIENT_PLATFORM_RETAIL_TERMINAL;
import static com.lc.df.service.utils.Constants.GVC_CLIENT_TYPE_SPORTSBOOK;
import static com.lc.df.service.utils.Constants.IMS_CLIENT_PLATFORM_RETAIL_OTC;
import static com.lc.df.service.utils.Constants.IMS_CLIENT_TYPE_RETAIL;
import static com.lc.df.service.utils.Constants.MASK_FIELD;

@Component
public class Utils {

    @Autowired
    DataFabricLogger log;

    @Value("${module.env}")
    private String env;

    private static DecimalFormat decimalFormatTwoDigit = new DecimalFormat("#.##");

    public String decodeMessage(String encodedText, String appName) {
        PEMReader pemReader;
        try {
            Security.addProvider(new BouncyCastleProvider());
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream in = classloader.getResourceAsStream("mykey.pem");
            String privateKey = CharStreams.toString(new InputStreamReader(in, "UTF-8"));

            pemReader = new PEMReader(new StringReader(privateKey));
            KeyPair key = (KeyPair)pemReader.readObject();
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(2, key.getPrivate());
            pemReader.close();

            byte[] cipheredBytes = Base64.decodeBase64(encodedText);
            byte[] decrypted = cipher.doFinal(cipheredBytes);
            return new String(decrypted, "UTF-8");
        } catch ( NoSuchAlgorithmException | BadPaddingException | InvalidKeyException
                | NoSuchPaddingException | IOException | IllegalBlockSizeException e) {
            log.log(appName, Level.ERROR, ExceptionUtils.getStackTrace(e), DF);
            return null;
        }
    }

    public String decodePin(String encodedText, String appName) {
        if (MASK_FIELD.equals(encodedText)) {
            return  encodedText;
        }
        String decodedMsg = decodeMessage(encodedText, appName);
        if (StringUtils.isNotBlank(decodedMsg) && decodedMsg.length() > 4) {
            return decodedMsg.substring(0, 4);
        }
        return null;
    }

    public String transformPenceToPounds(String pence) {
        BigDecimal penceValue = new BigDecimal(pence);
        return decimalFormatTwoDigit.format(penceValue.divide(new BigDecimal(100)));
    }

    public String getClientPlatform() {
        return GVC_CLIENT_PLATFORM_RETAIL_TERMINAL;
    }

    public String getClientType() {
        return GVC_CLIENT_TYPE_SPORTSBOOK;
    }
}
