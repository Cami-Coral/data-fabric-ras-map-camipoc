package com.lc.df.service.balance;

import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;

public interface BalanceService {

    BalanceResponse getPlayerBalanceByCardNumber(UserBalanceCommand userBalanceCommand);
    BalanceResponse getPlayerBalanceByUsername(UserBalanceCommand userBalanceCommand, String userName);
}
