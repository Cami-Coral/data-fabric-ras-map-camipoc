package com.lc.df.service.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ImsErrorException  extends RuntimeException {

    private final String code;
    private final String message;

}

