package com.lc.df.service.utils;

public class Constants {

    private Constants() {}

    public static final String API_ROOT = "/v4/ras-proxy-api";
    public static final String DF = "DF";
    public static final String IMS = "IMS";
    public static final String MASK_FIELD = "*****";
    public static final String CURRENCY_GBP = "GBP";

    public static final String PARAM_LOCALE = "locale";
    public static final String LOCALE_EN_GB = "en-GB";
    public static final String PARAM_API_KEY = "api-key";
    public static final String PARAM_CARD_NUMBER = "cardid";

    public static final String X_CLIENT_REQUEST_ID = "X-Client-Request-ID";
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";
    public static final String X_LC_CID = "X-LC-CID";

    public static final String MESSAGE_SUCCESS = "SUCCESS";
    public static final String MESSAGE_INSUFFICIENT_FUNDS = "INSUFFICIENT_FUNDS";
    public static final String MESSAGE_AUTHENTICATED = "AUTHENTICATED";
    public static final String MESSAGE_NOT_REGISTRED_CARD = "NON_REGISTERED_CARD";
    public static final String MESSAGE_NOT_AUTHENTICATED = "NOT_AUTHENTICATED";

    public static final String LOG_TYPE_REQUEST = "REQUEST";
    public static final String LOG_TYPE_RESPONSE = "RESPONSE";
    public static final String LOG_TYPE_ERROR = "ERROR";
    public static final String LOG_STATUS_PREP_REQ = "PREPARING REQUEST";
    public static final String LOG_SELF = "SELF";
    public static final String LOG_STATUS_OK = "OK";
    public static final String LOG_STATUS_ERROR = "ERROR";
    public static final String NA = "N/A";


    /* IMS Constants */
    public static final String IMS_HEADER_X_MESSAGE_ID = "X-Message-Id";
    public static final String IMS_HEADER_X_CASINO_NAME = "X-Casinoname";
    public static final String IMS_HEADER_X_IDENTITY_TYPE = "X-Identity-Type";
    public static final String X_IDENTITY_TYPE_CARD_NUMBER_VALUE = "IdTokenIdentityByCasinoAndRFIDPrintedTokenCode";
    public static final String X_IDENTITY_TYPE_CASINO_AND_USERNAME_VALUE = "PlayerIdentityByCasinoAndUsername";
    public static final String X_IDENTITY_TYPE_CASINO_AND_RFID_PRINTED_TOKEN_VALUE = "PlayerIdentityByCasinoAndRFIDPrintedTokenCode";
    public static final String IMS_HEADER_X_AUTH_TYPE = "X-Auth-Type";
    public static final String X_AUTH_TYPE_DEFAULT_VALUE = "AuthenticationByTrustedConnection";
    public static final String X_AUTH_TYPE_RFID_PRINTED_TOKEN = "AuthenticationByRFIDPrintedTokenCodeAndPIN";
    public static final String IMS_HEADER_X_ID_TOKEN_RFID_PRINTED_TOKEN_CODE = "X-IdToken-RFIDPrintedTokenCode";
    public static final String IMS_HEADER_X_PLAYER_USERNAME = "X-Player-Username";
    public static final String IMS_HEADER_X_PLAYER_RFID_PRINTED_TOKEN_CODE = "X-Player-RFIDPrintedTokenCode";
    public static final String IMS_HEADER_X_AUTH_PIN = "X-Auth-PIN";
    public static final String IMS_CLIENT_PLATFORM_RETAIL_OTC = "retailotc";
    public static final String GVC_CLIENT_PLATFORM_RETAIL_TERMINAL = "retailotc";
    public static final String IMS_CLIENT_TYPE_RETAIL = "retail";
    public static final String GVC_CLIENT_TYPE_SPORTSBOOK = "retail";
    public static final String IMS_ACTION_TYPE_PLACE_BET = "sb_otc_bet";
    public static final String IMS_ACTION_TYPE_CANCEL_BET = "sb_otc_bet_cancel";
    public static final String IMS_ACTION_TYPE_SETTLE_BET_WIN = "sb_otc_win";
    public static final String IMS_ACTION_TYPE_SETTLE_BET_VOID = "sb_otc_bet_void";
    public static final String IMS_ACTION_TYPE_UNSETTLE_BET = "sb_otc_unsettle";
    public static final String IMS_ONE = "1";
    public static final String IMS_LANG_EN = "EN";
    public static final String IMS_DEVICE_TYPE_PC = "PC";

    public static final String IMS_BALANCE_RUNNING_PLAYABLE = "running_playable_balance";
    public static final String IMS_BALANCE_OTC_GAMING = "otc_gaming_balance";
    public static final String IMS_BALANCE_OTC_WITHDRAWABLE = "otc_withdrawable_balance";

    public static final String IMS_ERROR_CODE_MISSING_FOUNDS = "UMS-108";
    public static final String IMS_ERROR_CODE_USER_NOT_FOUND = "UMS-107";

    public static final String ENV_PROD = "prod";




}
