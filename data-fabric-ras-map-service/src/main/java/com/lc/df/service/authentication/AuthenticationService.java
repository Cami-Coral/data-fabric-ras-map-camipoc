package com.lc.df.service.authentication;

import com.lc.df.ras.authentication.request.AuthenticationRequestCommand;
import com.lc.df.ras.authentication.response.AuthenticationResponse;

public interface AuthenticationService {

    AuthenticationResponse routeRASAuthenticationToDF(AuthenticationRequestCommand authenticationRequest);

}
