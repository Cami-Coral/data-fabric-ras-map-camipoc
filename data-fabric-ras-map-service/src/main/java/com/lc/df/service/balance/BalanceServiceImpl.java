package com.lc.df.service.balance;

import ch.qos.logback.classic.Level;
import com.lc.df.ras.Headers;
import com.lc.df.ras.balance.request.CardRequest;
import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.logging.DataFabricLogger;
import com.lc.df.playtech.DynamicBalance;
import com.lc.df.playtech.GetDynamicBalancesRequest;
import com.lc.df.playtech.GetDynamicBalancesResponse;
import com.lc.df.service.username.UsernameService;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.lc.df.service.utils.Constants.IMS_BALANCE_OTC_GAMING;
import static com.lc.df.service.utils.Constants.IMS_BALANCE_OTC_WITHDRAWABLE;
import static com.lc.df.service.utils.Constants.IMS_BALANCE_RUNNING_PLAYABLE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_AUTH_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_CASINO_NAME;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_IDENTITY_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_MESSAGE_ID;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_PLAYER_USERNAME;
import static com.lc.df.service.utils.Constants.LOG_SELF;
import static com.lc.df.service.utils.Constants.LOG_STATUS_ERROR;
import static com.lc.df.service.utils.Constants.LOG_STATUS_OK;
import static com.lc.df.service.utils.Constants.LOG_STATUS_PREP_REQ;
import static com.lc.df.service.utils.Constants.LOG_TYPE_ERROR;
import static com.lc.df.service.utils.Constants.LOG_TYPE_REQUEST;
import static com.lc.df.service.utils.Constants.LOG_TYPE_RESPONSE;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static com.lc.df.service.utils.Constants.NA;
import static com.lc.df.service.utils.Constants.X_AUTH_TYPE_DEFAULT_VALUE;
import static com.lc.df.service.utils.Constants.X_IDENTITY_TYPE_CASINO_AND_USERNAME_VALUE;

@Service
public class BalanceServiceImpl implements BalanceService {

    @Autowired
    private UsernameService usernameService;

    @Autowired
    private RestTemplate imsSSLRestTemplate;

    @Autowired
    DataFabricLogger log;

    private List<String> balanceTypes = Arrays.asList(IMS_BALANCE_RUNNING_PLAYABLE, IMS_BALANCE_OTC_GAMING, IMS_BALANCE_OTC_WITHDRAWABLE);

    @Override
    public BalanceResponse getPlayerBalanceByCardNumber(UserBalanceCommand userBalanceCommand) {
        try {
            GetIdTokenInfoResponse tokenInfoResponse = usernameService.getUsernameResponseByCardNumber(UsernameRequestCommand.builder()
                                                        .appName(userBalanceCommand.getAppName())
                                                        .headers(userBalanceCommand.getHeaders())
                                                        .cardNumber(Optional.ofNullable(userBalanceCommand.getCardRequest())
                                                                .map(CardRequest::getCardNumber)
                                                                .orElse(null))
                                                        .locale(userBalanceCommand.getLocale())
                                                        .url(userBalanceCommand.getImsTokenInfoUrl())
                                                        .build());

            String userName = tokenInfoResponse.getUsername();
            return getPlayerBalanceByUsername(userBalanceCommand, userName);

        } catch (Exception ex) {
            log.log(userBalanceCommand.getAppName(), Level.ERROR, "IMS - Get Player Balance - ".concat(ExceptionUtils.getStackTrace(ex)),
                    LOG_TYPE_ERROR, userBalanceCommand.getHeaders().getXLcCid(),
                    userBalanceCommand.getHeaders().getXClientRequestId(), userBalanceCommand.getHeaders().getXForwardedFor(),
                    null, userBalanceCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
            return BalanceResponse.builder()
                    .resultMessage(ExceptionUtils.getMessage(ex))
                    .build();
        }
    }

    public BalanceResponse getPlayerBalanceByUsername(UserBalanceCommand userBalanceCommand, String userName) {
        String appName = userBalanceCommand.getAppName();

        HttpEntity<GetDynamicBalancesRequest> httpEntity = new HttpEntity<>(buildBalanceRequest(),
                createHeadersForIMS(userBalanceCommand.getHeaders(), userName));

        String timeKey = log.startTimer();
        log.log(appName, Level.INFO, httpEntity, LOG_TYPE_REQUEST, userBalanceCommand.getHeaders().getXLcCid(),
                userBalanceCommand.getHeaders().getXClientRequestId(), userBalanceCommand.getHeaders().getXForwardedFor(),
                null, LOG_SELF, userBalanceCommand.getUrl(), LOG_STATUS_PREP_REQ);

        GetDynamicBalancesResponse response = imsSSLRestTemplate.postForObject(userBalanceCommand.getUrl(),
                httpEntity,
                GetDynamicBalancesResponse.class);

        log.log(appName, Level.INFO, response, LOG_TYPE_RESPONSE, userBalanceCommand.getHeaders().getXLcCid(),
                userBalanceCommand.getHeaders().getXClientRequestId(), userBalanceCommand.getHeaders().getXForwardedFor(),
                log.stopTimer(timeKey), userBalanceCommand.getUrl(), LOG_SELF, LOG_STATUS_OK);

        Map<String, DynamicBalance> results = response.getBalances().getDynamicBalance().stream()
                .filter(dynamicBalance -> balanceTypes.contains(dynamicBalance.getBalanceType()))
                .collect(Collectors.toMap(DynamicBalance::getBalanceType, Function.identity()));

        return BalanceResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(response.getErrorCode().toString())
                .totalAccountBalance(getBalanceFromMap(results, IMS_BALANCE_RUNNING_PLAYABLE))
                .otcBettingBalance(getBalanceFromMap(results, IMS_BALANCE_OTC_GAMING))
                .otcWithdrawableBalance(getBalanceFromMap(results, IMS_BALANCE_OTC_WITHDRAWABLE))
                .build();
    }

    protected String getBalanceFromMap(Map<String, DynamicBalance> results, String key) {
        return Optional.ofNullable(results.get(key))
                .map(DynamicBalance::getBalance)
                .map(DynamicBalance.Balance::getAmount)
                .map(BigDecimal::toString)
                .orElse(NA);
    }

    protected GetDynamicBalancesRequest buildBalanceRequest() {
        GetDynamicBalancesRequest getDynamicBalancesRequest = new GetDynamicBalancesRequest();
        balanceTypes.forEach(type -> getDynamicBalancesRequest.getBalanceTypes().add(type));
        return  getDynamicBalancesRequest;
    }

    private HttpHeaders createHeadersForIMS(Headers headersUsername, String userName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(IMS_HEADER_X_IDENTITY_TYPE, X_IDENTITY_TYPE_CASINO_AND_USERNAME_VALUE);
        headers.add(IMS_HEADER_X_CASINO_NAME, Optional.ofNullable(headersUsername)
                .map(Headers::getCasinoName)
                .orElse(null));
        headers.add(IMS_HEADER_X_PLAYER_USERNAME, userName);
        headers.add(IMS_HEADER_X_MESSAGE_ID, Optional.ofNullable(headersUsername)
                .map(Headers::getXClientRequestId)
                .orElse(null));
        headers.add(IMS_HEADER_X_AUTH_TYPE, X_AUTH_TYPE_DEFAULT_VALUE);
        return  headers;
    }
}
