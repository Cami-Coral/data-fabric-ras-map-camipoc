package com.lc.df.service.username;

import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.error.model.DataFabricException;
import com.playtech.ims.GetIdTokenInfoResponse;

public interface UsernameService {

    String getUsernameByCardNumber(UsernameRequestCommand usernameRequestCommand) throws DataFabricException;
    GetIdTokenInfoResponse getUsernameResponseByCardNumber(UsernameRequestCommand usernameRequestCommand);

}
