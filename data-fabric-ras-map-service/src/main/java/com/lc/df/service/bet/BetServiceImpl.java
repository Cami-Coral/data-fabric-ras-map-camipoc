package com.lc.df.service.bet;

import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lc.df.ras.Headers;
import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;
import com.lc.df.ras.betting.request.BetRequest;
import com.lc.df.ras.betting.request.BetRequestCommand;
import com.lc.df.ras.betting.request.CancelBetRequest;
import com.lc.df.ras.betting.request.CancelBetRequestCommand;
import com.lc.df.ras.betting.request.CardDetails;
import com.lc.df.ras.betting.request.Money;
import com.lc.df.ras.betting.request.PlaceBetRequestCommand;
import com.lc.df.ras.betting.request.SettleBetRequest;
import com.lc.df.ras.betting.request.SettleBetRequestCommand;
import com.lc.df.ras.betting.request.ShopCode;
import com.lc.df.ras.betting.request.Slip;
import com.lc.df.ras.betting.request.UnsettleBetRequest;
import com.lc.df.ras.betting.request.UnsettleBetRequestCommand;
import com.lc.df.ras.betting.request.ims.Amount;
import com.lc.df.ras.betting.request.ims.ContextByClientParameters;
import com.lc.df.ras.betting.request.ims.ImsGeneralMoneyTransactionRequest;
import com.lc.df.ras.betting.request.ims.ImsWalletBatchRequest;
import com.lc.df.ras.betting.request.ims.ParentTransactionReference;
import com.lc.df.ras.betting.response.BetPlacementResult;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.ras.betting.response.ims.ImsGeneralMoneyTransactionResponse;
import com.lc.df.ras.betting.response.ims.ImsWalletBatchResponse;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.logging.DataFabricLogger;
import com.lc.df.service.balance.BalanceService;
import com.lc.df.service.exception.Error;
import com.lc.df.service.exception.ImsErrorException;
import com.lc.df.service.username.UsernameService;
import com.lc.df.service.utils.Utils;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.lc.df.service.utils.Constants.CURRENCY_GBP;
import static com.lc.df.service.utils.Constants.DF;
import static com.lc.df.service.utils.Constants.IMS;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_CANCEL_BET;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_PLACE_BET;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_SETTLE_BET_VOID;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_SETTLE_BET_WIN;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_UNSETTLE_BET;
import static com.lc.df.service.utils.Constants.IMS_ERROR_CODE_MISSING_FOUNDS;
import static com.lc.df.service.utils.Constants.IMS_ERROR_CODE_USER_NOT_FOUND;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_AUTH_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_CASINO_NAME;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_IDENTITY_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_MESSAGE_ID;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_PLAYER_USERNAME;
import static com.lc.df.service.utils.Constants.IMS_LANG_EN;
import static com.lc.df.service.utils.Constants.LOG_SELF;
import static com.lc.df.service.utils.Constants.LOG_STATUS_ERROR;
import static com.lc.df.service.utils.Constants.LOG_STATUS_OK;
import static com.lc.df.service.utils.Constants.LOG_STATUS_PREP_REQ;
import static com.lc.df.service.utils.Constants.LOG_TYPE_ERROR;
import static com.lc.df.service.utils.Constants.LOG_TYPE_REQUEST;
import static com.lc.df.service.utils.Constants.LOG_TYPE_RESPONSE;
import static com.lc.df.service.utils.Constants.MESSAGE_INSUFFICIENT_FUNDS;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static com.lc.df.service.utils.Constants.X_AUTH_TYPE_DEFAULT_VALUE;
import static com.lc.df.service.utils.Constants.X_IDENTITY_TYPE_CASINO_AND_USERNAME_VALUE;

@Service
public class BetServiceImpl implements BetService {

    @Autowired
    DataFabricLogger log;

    @Autowired
    RestTemplate imsSSLRestTemplate;

    @Autowired
    private UsernameService usernameService;

    @Autowired
    private BalanceService balanceService;

    @Autowired
    private Utils utils;

    @Autowired
    XmlMapper xmlMapper;

    @Override
    public BetPlacementResult placeBet(PlaceBetRequestCommand placeBetRequestCommand) throws DataFabricException {
        String usernameForPlaceBet = getUsernameForPlaceBet(placeBetRequestCommand);

        List<ImsGeneralMoneyTransactionRequest> imsGeneralMoneyTransactionRequests = placeBetRequestCommand.getBetRequest().getSlips().stream()
                .map(slip -> ImsGeneralMoneyTransactionRequest.builder()
                        .description(slip.getDescription())
                        .username(usernameForPlaceBet)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(utils.getClientPlatform())
                                .clientType(utils.getClientType())
                                .deviceType(placeBetRequestCommand.getBetRequest().getDeviceType())
                                .languageCode(IMS_LANG_EN)
                                .venue(Optional.ofNullable(placeBetRequestCommand.getBetRequest().getShopCode())
                                        .map(ShopCode::getCode)
                                        .orElse(null))
                                .build())
                        .transactionCode(slip.getSlipNumber())
                        .transactionDate(slip.getDate())
                        .amount(Amount.builder()
                                .amount(getMoneyAsNegativeInPounds(slip.getAmount().getPence()))
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(utils.getClientPlatform())
                        .clientType(utils.getClientType())
                        .actionType(IMS_ACTION_TYPE_PLACE_BET)
                        .build())
                .collect(Collectors.toList());

        CallResult callResult = prepareImsBetWalletForPlaceBet(placeBetRequestCommand, usernameForPlaceBet, imsGeneralMoneyTransactionRequests);
        return BetPlacementResult.betPlacementResultBuilder()
                .resultMessage(callResult.getResultMessage())
                .resultCode(callResult.getResultCode())
                .username(callResult.getUsername())
                .missingFunds(callResult.getMissingFunds())
                .build();
    }

    @Override
    public CallResult cancelBet(CancelBetRequestCommand cancelBetRequestCommand) throws DataFabricException {
        CancelBetRequest cancelBetRequest = cancelBetRequestCommand.getCancelBetRequest();
        ImsGeneralMoneyTransactionRequest generalMoneyTransactionRequest = ImsGeneralMoneyTransactionRequest.builder()
                .username(cancelBetRequest.getUsername())
                .contextByClientParameters(ContextByClientParameters.builder()
                        .clientPlatform(utils.getClientPlatform())
                        .clientType(utils.getClientType())
                        .deviceType(cancelBetRequest.getDeviceType())
                        .languageCode(IMS_LANG_EN)
                        .build())
                .transactionCode(cancelBetRequest.getSlip().getSlipNumber())
                .transactionDate(cancelBetRequest.getSlip().getDate())
                .amount(Amount.builder()
                        .amount(utils.transformPenceToPounds(cancelBetRequest.getSlip().getAmount().getPence()))
                        .currencyCode(CURRENCY_GBP)
                        .build())
                .clientPlatform(utils.getClientPlatform())
                .clientType(utils.getClientType())
                .actionType(IMS_ACTION_TYPE_CANCEL_BET)
                .parentTransactionReference(ParentTransactionReference.builder()
                        .transactionCode(cancelBetRequest.getParentTransactionCode())
                        .transactionDate(cancelBetRequest.getParentTransactionDate())
                        .actionType(IMS_ACTION_TYPE_PLACE_BET)
                        .build())
                .description(cancelBetRequest.getSlip().getDescription())
                .build();
        return prepareImsBetWallet(cancelBetRequestCommand, cancelBetRequest.getUsername(), Arrays.asList(generalMoneyTransactionRequest));
    }

    @Override
    public CallResult settleBet(SettleBetRequestCommand settleBetRequestCommand) throws DataFabricException {

        SettleBetRequest settleBetRequest = settleBetRequestCommand.getSettleBetRequest();
        List<ImsGeneralMoneyTransactionRequest> imsGeneralMoneyTransactionRequests = new ArrayList<>();

        if (!"0".equals(settleBetRequest.getWinAmount().getPence())) {
            imsGeneralMoneyTransactionRequests.add(buildSettleBetRequest(settleBetRequest, true));
        }

        if (!"0".equals(settleBetRequest.getVoidAmount().getPence())) {
            imsGeneralMoneyTransactionRequests.add(buildSettleBetRequest(settleBetRequest, false));
        }

        return prepareImsBetWallet(settleBetRequestCommand, settleBetRequest.getUsername(), imsGeneralMoneyTransactionRequests);
    }

    @Override
    public CallResult unsettleBet(UnsettleBetRequestCommand unsettleBetRequestCommand) throws DataFabricException {
        UnsettleBetRequest unsettleBetRequest = unsettleBetRequestCommand.getUnsettleBetRequest();
        ImsGeneralMoneyTransactionRequest imsGeneralMoneyTransactionRequest = ImsGeneralMoneyTransactionRequest.builder()
                .username(unsettleBetRequest.getUsername())
                .contextByClientParameters(ContextByClientParameters.builder()
                        .clientPlatform(utils.getClientPlatform())
                        .clientType(utils.getClientType())
                        .deviceType(unsettleBetRequest.getDeviceType())
                        .languageCode(IMS_LANG_EN)
                        .build())
                .transactionCode(unsettleBetRequest.getSlipNumber())
                .transactionDate(unsettleBetRequest.getDate())
                .amount(Amount.builder()
                        .amount(getMoneyAsNegativeInPounds(unsettleBetRequest.getAmount().getPence()))
                        .currencyCode(CURRENCY_GBP)
                        .build())
                .clientPlatform(utils.getClientPlatform())
                .clientType(utils.getClientType())
                .actionType(IMS_ACTION_TYPE_UNSETTLE_BET)
                .parentTransactionReference(ParentTransactionReference.builder()
                        .transactionCode(unsettleBetRequest.getParentTransactionCode())
                        .transactionDate(unsettleBetRequest.getParentTransactionDate())
                        .actionType(IMS_ACTION_TYPE_PLACE_BET)
                        .build())
                .build();
        return prepareImsBetWallet(unsettleBetRequestCommand, unsettleBetRequest.getUsername(), Arrays.asList(imsGeneralMoneyTransactionRequest));
    }

    protected CallResult prepareImsBetWalletForPlaceBet(PlaceBetRequestCommand placeBetRequestCommand, String usernameForPleaceBet, List<ImsGeneralMoneyTransactionRequest> imsGeneralMoneyTransactionRequests) throws DataFabricException {
        if (imsGeneralMoneyTransactionRequests.isEmpty()) {
            throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, "Expected a list of requests built from slips, but got 0 requests.", DF);
        }
        String appName = placeBetRequestCommand.getAppName();
        try {
            return imsWalletBatchCall(placeBetRequestCommand, usernameForPleaceBet, imsGeneralMoneyTransactionRequests, appName);
        } catch (ImsErrorException ex) {
            log.log(appName, Level.ERROR,"Prepare IMS Wallet Batch Exception: " + ex.getCode() + "|" + ex.getMessage(),
                    LOG_TYPE_ERROR, placeBetRequestCommand.getHeaders().getXLcCid(),
                    placeBetRequestCommand.getHeaders().getXClientRequestId(), placeBetRequestCommand.getHeaders().getXForwardedFor(),
                    null, placeBetRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);

            if(IMS_ERROR_CODE_MISSING_FOUNDS.equalsIgnoreCase(ex.getCode())) {
                log.log(appName, Level.ERROR,"Start calculating missing funds: " + ex.getCode(),
                        LOG_TYPE_RESPONSE, placeBetRequestCommand.getHeaders().getXLcCid(),
                        placeBetRequestCommand.getHeaders().getXClientRequestId(), placeBetRequestCommand.getHeaders().getXForwardedFor(),
                        null, placeBetRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
                return calculateMissingFounds(placeBetRequestCommand, usernameForPleaceBet);
            }

            if (IMS_ERROR_CODE_USER_NOT_FOUND.equalsIgnoreCase(ex.getCode())) {
                throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, String.format("Username [%s] was not found in IMS", usernameForPleaceBet), IMS);
            }

            throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, String.format("IMS returned unexpected error code: [%s]", ex.getCode()), IMS);
        }
    }

    protected CallResult prepareImsBetWallet(BetRequestCommand betRequestCommand, String username, List<ImsGeneralMoneyTransactionRequest> imsGeneralMoneyTransactionRequests) throws DataFabricException {
        if (imsGeneralMoneyTransactionRequests.isEmpty()) {
            throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, "Expected a list of requests built from slips, but got 0 requests.", DF);
        }
        String appName = betRequestCommand.getAppName();
        try {
            return imsWalletBatchCall(betRequestCommand, username, imsGeneralMoneyTransactionRequests, appName);
        } catch (ImsErrorException ex) {
            log.log(appName, Level.ERROR,"IMS Wallet Batch Exception: " + ExceptionUtils.getStackTrace(ex),
                    LOG_TYPE_RESPONSE, betRequestCommand.getHeaders().getXLcCid(),
                    betRequestCommand.getHeaders().getXClientRequestId(), betRequestCommand.getHeaders().getXForwardedFor(),
                    null, betRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
            if (IMS_ERROR_CODE_USER_NOT_FOUND.equalsIgnoreCase(ex.getCode())) {
                throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, String.format("Username [%s] was not found in IMS", username), IMS);
            }

            throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, String.format("IMS returned unexpected error code: [%s]", ex.getCode()), IMS);
        }
    }

    protected String getUsernameForPlaceBet(PlaceBetRequestCommand placeBetRequestCommand) throws DataFabricException {
        UsernameRequestCommand usernameRequestCommand = UsernameRequestCommand.builder()
                .headers(placeBetRequestCommand.getHeaders())
                .appName(placeBetRequestCommand.getAppName())
                .url(placeBetRequestCommand.getUrlImsUsername())
                .locale(placeBetRequestCommand.getLocale())
                .cardNumber(Optional.ofNullable(placeBetRequestCommand.getBetRequest())
                        .map(BetRequest::getCardLogin)
                        .map(CardDetails::getCardNumber)
                        .orElse(null))
                .build();

        GetIdTokenInfoResponse usernameTokenResponse = usernameService.getUsernameResponseByCardNumber(usernameRequestCommand);

        return Optional.ofNullable(usernameTokenResponse)
                .map(GetIdTokenInfoResponse::getUsername)
                .orElseThrow(() -> new DataFabricException(DataFabricErrorCodes.MISSING_USERNAME,
                        String.format("Username cannot be found in IMS for card number [%s].",
                                Optional.ofNullable(placeBetRequestCommand.getBetRequest())
                                        .map(BetRequest::getCardLogin)
                                        .map(CardDetails::getCardNumber)
                                        .orElse(null)),
                        IMS));
    }

    private CallResult imsWalletBatchCall(BetRequestCommand betRequestCommand, String username, List<ImsGeneralMoneyTransactionRequest> imsGeneralMoneyTransactionRequests, String appName) {
        ImsWalletBatchRequest imsWalletBatchRequest = ImsWalletBatchRequest.builder()
                .username(username)
                .generalMoneyTransactionRequests(imsGeneralMoneyTransactionRequests)
                .build();
        HttpEntity<ImsWalletBatchRequest> httpEntity = new HttpEntity<>(imsWalletBatchRequest, createHeadersForImsWalletBatchRequest(betRequestCommand, username));

        String timeKey = log.startTimer();
        log.log(appName, Level.INFO, httpEntity, LOG_TYPE_REQUEST, betRequestCommand.getHeaders().getXLcCid(),
                betRequestCommand.getHeaders().getXClientRequestId(), betRequestCommand.getHeaders().getXForwardedFor(),
                null, LOG_SELF, betRequestCommand.getUrl(), LOG_STATUS_PREP_REQ);

        try {
            ImsWalletBatchResponse response = imsSSLRestTemplate.postForObject(betRequestCommand.getUrl(),
                    httpEntity, ImsWalletBatchResponse.class);
            log.log(appName, Level.INFO, response, LOG_TYPE_RESPONSE, betRequestCommand.getHeaders().getXLcCid(),
                    betRequestCommand.getHeaders().getXClientRequestId(), betRequestCommand.getHeaders().getXForwardedFor(),
                    log.stopTimer(timeKey), betRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_OK);

            return CallResult.builder()
                    .resultMessage(MESSAGE_SUCCESS)
                    .resultCode(getErrorCodeFromResponse(response))
                    .username(username)
                    .build();

        } catch (HttpStatusCodeException ex) {
            String exBody = Optional.ofNullable(ex.getResponseBodyAsString()).orElse("");
            log.log(appName, Level.ERROR,"IMS Wallet Batch Exception: " + ex.getResponseBodyAsString() + "|" + exBody + "|" + ExceptionUtils.getStackTrace(ex),
                    LOG_TYPE_ERROR, betRequestCommand.getHeaders().getXLcCid(),
                    betRequestCommand.getHeaders().getXClientRequestId(), betRequestCommand.getHeaders().getXForwardedFor(),
                    null, betRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
            /*Error error = null;
            try {
                 error = xmlMapper.readValue(ex.getResponseBodyAsString(), Error.class);
            } catch (IOException e) {
                log.log(appName, Level.ERROR,"IMS Wallet Batch Exception: Unable to unmarshal IMS Error response" + ExceptionUtils.getStackTrace(e),
                        LOG_TYPE_ERROR, betRequestCommand.getHeaders().getXLcCid(),
                        betRequestCommand.getHeaders().getXClientRequestId(), betRequestCommand.getHeaders().getXForwardedFor(),
                        null, betRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
            }
            throw new ImsErrorException(Optional.ofNullable(error)
                                                .map(err -> err.getCode())
                                                .orElse("000"),
                                        Optional.ofNullable(error)
                                                .map(err -> err.getDescription())
                                                .orElse(ex.getMessage()));

             */
            //String errMsg = Optional.ofNullable()
            if (exBody.contains("UMS-108")){
                log.log(appName, Level.ERROR,"IMS Insufficent Funds: " + ex.getResponseBodyAsString(),
                        LOG_TYPE_ERROR, betRequestCommand.getHeaders().getXLcCid(),
                        betRequestCommand.getHeaders().getXClientRequestId(), betRequestCommand.getHeaders().getXForwardedFor(),
                        null, betRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
                throw new ImsErrorException(IMS_ERROR_CODE_MISSING_FOUNDS, ex.getMessage());
            }
            throw new ImsErrorException("000", ex.getMessage());
        }
    }

    private String getErrorCodeFromResponse(ImsWalletBatchResponse response) {
        return Optional.of(response)
                .map(ImsWalletBatchResponse::getGeneralMoneyTransactionResponses)
                .filter(Objects::nonNull)
                .map( list -> list.stream()
                        .filter(resp -> !"0".equals(resp.getErrorCode()))
                        .findFirst()
                        .map(ImsGeneralMoneyTransactionResponse::getErrorCode)
                        .orElse(null))
                .orElse(response.getErrorCode());
    }

    private String getMoneyAsNegativeInPounds(String pence) {
        int negativePence = Math.abs(Integer.valueOf(pence)) * -1;
        BigDecimal amountInPence = new BigDecimal(negativePence);
        BigDecimal divisor = new BigDecimal(100);
        BigDecimal amountInPounds = amountInPence.divide(divisor).setScale(2);
        return amountInPounds.toString();
    }

    private CallResult calculateMissingFounds(PlaceBetRequestCommand placeBetRequestCommand, String usernameForPleaceBet) throws DataFabricException {
        try {
            int totalAmountForSplips = placeBetRequestCommand.getBetRequest().getSlips().stream()
                    .map(Slip::getAmount)
                    .map(Money::getPence)
                    .mapToInt(Integer::parseInt)
                    .sum();

            BalanceResponse balanceResponse = balanceService.getPlayerBalanceByUsername(UserBalanceCommand.builder()
                            .headers(placeBetRequestCommand.getHeaders())
                            .appName(placeBetRequestCommand.getAppName())
                            .locale(placeBetRequestCommand.getLocale())
                            .url(placeBetRequestCommand.getUrlImsUserBalance())
                            .build(),
                    usernameForPleaceBet);

            String totalAmount = balanceResponse.getTotalAccountBalance();
            Integer totalAmountBalance = new BigDecimal(totalAmount).intValue();

            return CallResult.builder()
                    .resultMessage(MESSAGE_INSUFFICIENT_FUNDS)
                    .resultCode("31")
                    .username(usernameForPleaceBet)
                    .missingFunds(Money.builder()
                            .pence(String.valueOf(totalAmountForSplips - totalAmountBalance))
                            .build())
                    .build();
        } catch (Exception ex) {
            throw new DataFabricException(DataFabricErrorCodes.SOMETHING_WENT_WRONG, ExceptionUtils.getStackTrace(ex), DF);
        }
    }

    private ImsGeneralMoneyTransactionRequest buildSettleBetRequest(SettleBetRequest settleBetRequest, boolean isWin) {
        return ImsGeneralMoneyTransactionRequest.builder()
                .username(settleBetRequest.getUsername())
                .contextByClientParameters(ContextByClientParameters.builder()
                        .clientPlatform(utils.getClientPlatform())
                        .clientType(utils.getClientType())
                        .deviceType(settleBetRequest.getDeviceType())
                        .languageCode(IMS_LANG_EN)
                        .build())
                .transactionCode(settleBetRequest.getSlipNumber())
                .transactionDate(settleBetRequest.getDate())
                .amount(Amount.builder()
                        .amount(isWin ? utils.transformPenceToPounds(settleBetRequest.getWinAmount().getPence())
                                : utils.transformPenceToPounds(settleBetRequest.getVoidAmount().getPence()))
                        .currencyCode(CURRENCY_GBP)
                        .build())
                .clientPlatform(utils.getClientPlatform())
                .clientType(utils.getClientType())
                .actionType(isWin ? IMS_ACTION_TYPE_SETTLE_BET_WIN : IMS_ACTION_TYPE_SETTLE_BET_VOID)
                .parentTransactionReference(ParentTransactionReference.builder()
                        .transactionCode(settleBetRequest.getParentTransactionCode())
                        .transactionDate(settleBetRequest.getParentTransactionDate())
                        .actionType(IMS_ACTION_TYPE_PLACE_BET)
                        .build())
                .description(settleBetRequest.getDescription())
                .build();
    }

    private HttpHeaders createHeadersForImsWalletBatchRequest(BetRequestCommand betRequestCommand, String username) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(IMS_HEADER_X_MESSAGE_ID, Optional.ofNullable(betRequestCommand.getHeaders())
                .map(Headers::getXLcCid)
                .orElse(null));
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.add(IMS_HEADER_X_AUTH_TYPE, X_AUTH_TYPE_DEFAULT_VALUE);
        headers.add(IMS_HEADER_X_IDENTITY_TYPE, X_IDENTITY_TYPE_CASINO_AND_USERNAME_VALUE);
        headers.add(IMS_HEADER_X_CASINO_NAME, Optional.ofNullable(betRequestCommand.getHeaders())
                .map(Headers::getCasinoName)
                .orElse(null));
        headers.add(IMS_HEADER_X_PLAYER_USERNAME, username);
        return headers;
    }
}
