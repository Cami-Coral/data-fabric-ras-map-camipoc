package com.lc.df.service.authentication;

import ch.qos.logback.classic.Level;
import com.lc.df.ras.Headers;
import com.lc.df.ras.authentication.request.AuthenticationRequest;
import com.lc.df.ras.authentication.request.AuthenticationRequestCommand;
import com.lc.df.ras.authentication.request.CardLogin;
import com.lc.df.ras.authentication.response.AuthenticationResponse;
import com.lc.df.logging.DataFabricLogger;
import com.lc.df.service.utils.Utils;
import com.playtech.ims.LoginRequest;
import com.playtech.ims.LoginResponse;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

import static com.lc.df.service.utils.Constants.IMS_DEVICE_TYPE_PC;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_AUTH_PIN;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_AUTH_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_CASINO_NAME;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_IDENTITY_TYPE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_ID_TOKEN_RFID_PRINTED_TOKEN_CODE;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_MESSAGE_ID;
import static com.lc.df.service.utils.Constants.IMS_HEADER_X_PLAYER_RFID_PRINTED_TOKEN_CODE;
import static com.lc.df.service.utils.Constants.IMS_LANG_EN;
import static com.lc.df.service.utils.Constants.IMS_ONE;
import static com.lc.df.service.utils.Constants.LOG_SELF;
import static com.lc.df.service.utils.Constants.LOG_STATUS_ERROR;
import static com.lc.df.service.utils.Constants.LOG_STATUS_OK;
import static com.lc.df.service.utils.Constants.LOG_STATUS_PREP_REQ;
import static com.lc.df.service.utils.Constants.LOG_TYPE_ERROR;
import static com.lc.df.service.utils.Constants.LOG_TYPE_REQUEST;
import static com.lc.df.service.utils.Constants.LOG_TYPE_RESPONSE;
import static com.lc.df.service.utils.Constants.MASK_FIELD;
import static com.lc.df.service.utils.Constants.MESSAGE_AUTHENTICATED;
import static com.lc.df.service.utils.Constants.MESSAGE_NOT_AUTHENTICATED;
import static com.lc.df.service.utils.Constants.MESSAGE_NOT_REGISTRED_CARD;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static com.lc.df.service.utils.Constants.X_AUTH_TYPE_RFID_PRINTED_TOKEN;
import static com.lc.df.service.utils.Constants.X_IDENTITY_TYPE_CASINO_AND_RFID_PRINTED_TOKEN_VALUE;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    DataFabricLogger log;

    @Autowired
    private RestTemplate imsSSLRestTemplate;

    @Autowired
    private Utils utils;

    @Override
    public AuthenticationResponse routeRASAuthenticationToDF(AuthenticationRequestCommand authenticationRequestCommand) {

        String appName = authenticationRequestCommand.getAppName();

        try {
            HttpEntity<LoginRequest> httpEntity = new HttpEntity<>(buildLoginAuthorisationRequest(), createHeadersForLoginRequest(authenticationRequestCommand));

            String timeKey = log.startTimer();
            obfuscateSensitiveDataInCommand(authenticationRequestCommand);
            log.log(appName, Level.INFO, new HttpEntity<>(buildLoginAuthorisationRequest(), createHeadersForLoginRequest(authenticationRequestCommand)),
                    LOG_TYPE_REQUEST, authenticationRequestCommand.getHeaders().getXLcCid(),
                    authenticationRequestCommand.getHeaders().getXClientRequestId(), authenticationRequestCommand.getHeaders().getXForwardedFor(),
                    null, LOG_SELF, authenticationRequestCommand.getUrl(), LOG_STATUS_PREP_REQ);

            LoginResponse response = imsSSLRestTemplate.postForObject(authenticationRequestCommand.getUrl(),
                    httpEntity, LoginResponse.class);

            log.log(appName, Level.INFO, response, LOG_TYPE_RESPONSE, authenticationRequestCommand.getHeaders().getXLcCid(),
                    authenticationRequestCommand.getHeaders().getXClientRequestId(), authenticationRequestCommand.getHeaders().getXForwardedFor(),
                    log.stopTimer(timeKey), authenticationRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_OK);

            return AuthenticationResponse.builder()
                    .resultMessage(MESSAGE_SUCCESS)
                    .resultCode(0)
                    .authenticationStatus(MESSAGE_AUTHENTICATED)
                    .build();
        } catch (Exception ex) {
            log.log(appName, Level.ERROR, "DF - Authentication - ".concat(ExceptionUtils.getStackTrace(ex)),
                    LOG_TYPE_ERROR, authenticationRequestCommand.getHeaders().getXLcCid(),
                    authenticationRequestCommand.getHeaders().getXClientRequestId(), authenticationRequestCommand.getHeaders().getXForwardedFor(),
                    null, authenticationRequestCommand.getUrl(), LOG_SELF, LOG_STATUS_ERROR);
            return handleCustomerLoginAPIException(ex);
        }
    }

    protected AuthenticationResponse handleCustomerLoginAPIException(Exception ex) {
        if (ex instanceof HttpClientErrorException) {
            HttpClientErrorException hcee = (HttpClientErrorException) ex;
            if (HttpStatus.UNAUTHORIZED.equals(hcee.getStatusCode())) {
                return AuthenticationResponse.builder()
                        .resultMessage(MESSAGE_SUCCESS)
                        .resultCode(0)
                        .authenticationStatus(MESSAGE_NOT_REGISTRED_CARD)
                        .build();
            }
        }

        return AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_AUTHENTICATED)
                .build();
    }

    private LoginRequest buildLoginAuthorisationRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setClientPlatform(utils.getClientPlatform());
        loginRequest.setClientType(utils.getClientType());
        loginRequest.setClientVersion(IMS_ONE);
        loginRequest.setRealMode(IMS_ONE);
        loginRequest.setLanguageCode(IMS_LANG_EN);
        loginRequest.setVenue(null);
        loginRequest.setDeviceType(IMS_DEVICE_TYPE_PC);
        loginRequest.setClientChannel(null);
        loginRequest.setUserAgent(null);
        return loginRequest;
    }

    private HttpHeaders createHeadersForLoginRequest(AuthenticationRequestCommand authenticationRequestCommand) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(IMS_HEADER_X_MESSAGE_ID, Optional.ofNullable(authenticationRequestCommand.getHeaders())
                .map(Headers::getXLcCid)
                .orElse(null));
        headers.add(IMS_HEADER_X_CASINO_NAME, Optional.ofNullable(authenticationRequestCommand.getHeaders())
                .map(Headers::getCasinoName)
                .orElse(null));
        headers.add(IMS_HEADER_X_IDENTITY_TYPE, X_IDENTITY_TYPE_CASINO_AND_RFID_PRINTED_TOKEN_VALUE);
        headers.add(IMS_HEADER_X_AUTH_TYPE, X_AUTH_TYPE_RFID_PRINTED_TOKEN);
        headers.add(IMS_HEADER_X_PLAYER_RFID_PRINTED_TOKEN_CODE, Optional.ofNullable(authenticationRequestCommand.getAuthenticationRequest())
                .map(AuthenticationRequest::getCardLogin)
                .map(CardLogin::getCardNumber)
                .orElse(null));
        headers.add(IMS_HEADER_X_ID_TOKEN_RFID_PRINTED_TOKEN_CODE, Optional.ofNullable(authenticationRequestCommand.getAuthenticationRequest())
                .map(AuthenticationRequest::getCardLogin)
                .map(CardLogin::getCardNumber)
                .orElse(null));
        headers.add(IMS_HEADER_X_AUTH_PIN, Optional.ofNullable(authenticationRequestCommand.getAuthenticationRequest())
                .map(AuthenticationRequest::getCardLogin)
                .map(CardLogin::getCardPIN)
                .map(pin -> utils.decodePin(pin, authenticationRequestCommand.getAppName()))
                .orElse(null));
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        headers.setContentType(MediaType.APPLICATION_XML);
        return headers;
    }

    private void obfuscateSensitiveDataInCommand(AuthenticationRequestCommand authenticationRequestCommand) {
        Optional.ofNullable(authenticationRequestCommand.getAuthenticationRequest())
                .ifPresent(authenticationRequest -> Optional.ofNullable(authenticationRequest.getCardLogin())
                                                            .ifPresent(cardLogin -> cardLogin.setCardPIN(MASK_FIELD)));
    }
}
