package com.lc.df.service.utils;

import com.lc.df.service.TestBase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
@RunWith(MockitoJUnitRunner.class)
public class UtilsTest extends TestBase {

    @InjectMocks
    Utils utils;

    private final String APP_NAME = "ras-map-api";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    private static final String STR_1234_ENCODED = "QvKq5bAHkSwEqAJ6l5lHR0h9tUMsUuOb9wfH4KnIUXwkgerEPm/IRu4DLwc5fw7AaC5tQS95pQm7pqse7VP3RhUASEOXwp8exPRZrrW3i6fkl/ABtJswhcom5fNjABv3WN5Usny9wasO/OAUDMaYOafUpUNYFOWT3M4Ni9qrioM=";
    private static final String STR_8523_ENCODED = "HbXAfsNUs51rkqOQne/mw8ykMC4n8VMI6cp2SqOAW1EXPhC1/sPlfAGAIps86+MtNd1voaYCSd2MZZAB7/KioJ6KJeJgjyBtJGAF87ghbs8EjAC0OdtHBIlD03zxSothNN1nwSAU3itKlnL+ugqyBtf4nZIJu4gH3ToxH+Ejz80=";


    @Test
    public void when1234MessageIsEncodedThenReturnMessagePlainTextSuccessfully() {
        assertEquals("123413/02/2019 16:48:52", utils.decodeMessage(STR_1234_ENCODED, APP_NAME));
    }

    @Test
    public void when8523MessageIsEncodedThenReturnMessagePlainTextSuccessfully() {
        assertEquals("852313/02/2019 16:48:24", utils.decodeMessage(STR_8523_ENCODED, APP_NAME));
    }

    @Test
    public void when1234MessageIsEncodedThenReturnPinInPlainTextSuccessfully() {
        assertEquals("1234", utils.decodePin(STR_1234_ENCODED, APP_NAME));
    }

    @Test
    public void when8523MessageIsEncodedThenReturnPinInPlainTextSuccessfully() {
        assertEquals("8523", utils.decodePin(STR_8523_ENCODED, APP_NAME));
    }

    @Test
    public void whenMessageIsNotCorrectThenReturnNULL() {
        assertEquals(null, utils.decodePin("123", APP_NAME));
    }

    @Test
    public void whenHundredPenceIsTransformedToPoundsThenReturnOne() {
        assertEquals("1", utils.transformPenceToPounds("100"));
    }

    @Test
    public void whenZeroIsTransformedToPoundsThenReturnZero() {
        assertEquals("0", utils.transformPenceToPounds("0"));
    }

    @Test
    public void when5980IsTransformedToPoundsThenReturn59PoundsAnd80Pence() {
        assertEquals("59.8", utils.transformPenceToPounds("5980"));
    }

    @Test
    public void when2PenceIsTransformedToPoundsThenReturn0PoundsAnd02Pence() {
        assertEquals("0.02", utils.transformPenceToPounds("2"));
    }

    @Test
    public void when12PenceIsTransformedToPoundsThenReturn0PoundsAnd12Pence() {
        assertEquals("0.12", utils.transformPenceToPounds("12"));
    }

    @Test
    public void when123PenceIsTransformedToPoundsThenReturn1PoundsAnd23Pence() {
        assertEquals("1.23", utils.transformPenceToPounds("123"));
    }

    @Test
    public void whenHundredPenceThenTransformInOnePound() {
        assertEquals("1", utils.transformPenceToPounds("100"));
    }

    @Test
    public void whenFiftyPenceThenTransformInZeroDotFivePound() {
        assertEquals("0.5", utils.transformPenceToPounds("50"));
    }

    @Test
    public void whenFiveHundredPenceThenTransformInFivePound() {
        assertEquals("5", utils.transformPenceToPounds("500"));
    }

}
