package com.lc.df.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lc.df.logging.DataFabricLogger;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;

public class TestBase {

    @Mock
    protected RestTemplate restTemplate;

    @Mock
    protected DataFabricLogger log;

    protected XmlMapper xmlMapper = new XmlMapper();

}
