package com.lc.df.service.username;

import com.lc.df.ras.Headers;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.service.TestBase;
import com.playtech.ims.Error;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.nio.charset.Charset;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsernameServiceTest extends TestBase {

    private final String CARD_NUMBER = "8098791524808183";
    private final String USERNAME = "username";
    private final String MESSAGE_ID_OK = "MSG-OK";
    private final String CASINO = "Casino1";

    @InjectMocks
    UsernameServiceImpl usernameService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCardNumberIsProvidedThenReturnUsername() throws Exception {
        UsernameRequestCommand request = buildUsernameRequestCommand();

        GetIdTokenInfoResponse expected = new GetIdTokenInfoResponse();
        expected.setUsername(USERNAME);
        expected.setMessageId(MESSAGE_ID_OK);
        GetIdTokenInfoResponse.AttachedPlayer attachedPlayer = new GetIdTokenInfoResponse.AttachedPlayer();
        attachedPlayer.setCasino(CASINO);
        attachedPlayer.setUsername(USERNAME);
        expected.setAttachedPlayer(attachedPlayer);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(expected);

        String response = usernameService.getUsernameByCardNumber(request);
        GetIdTokenInfoResponse actual = xmlMapper.readValue(response, GetIdTokenInfoResponse.class);

        assertEquals(expected.getUsername(), actual.getUsername());
        assertEquals(expected.getMessageId(), actual.getMessageId());
        assertEquals(expected.getAttachedPlayer().getUsername(),
                Optional.ofNullable(actual.getAttachedPlayer())
                            .map(GetIdTokenInfoResponse.AttachedPlayer::getUsername).get());
        assertEquals(expected.getAttachedPlayer().getCasino(),
                Optional.ofNullable(actual.getAttachedPlayer())
                        .map(GetIdTokenInfoResponse.AttachedPlayer::getCasino).get());
        assertEquals(expected.getMessageId(), actual.getMessageId());
    }

    @Test
    public void whenCardNumberIsProvidedAndIMSDoesntRespondThenSomethingWenTWrongException() throws Exception {
        UsernameRequestCommand request = buildUsernameRequestCommand();
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new RestClientException("IMS doesn't respond"));

        String response = usernameService.getUsernameByCardNumber(request);
        Error actual = xmlMapper.readValue(response, Error.class);

        assertEquals(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR), actual.getCode());
        assertEquals("IMS connectivity issues. Error: [IMS doesn't respond]", actual.getDescription());
    }

    @Test
    public void whenCardNumberIsProvidedAndThereIsAnIMSNotSuccessResponseThenMissingUsernameException() throws Exception {
        UsernameRequestCommand request = buildUsernameRequestCommand();
        HttpStatusCodeException httpStatusCodeException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, null, null,
                Charset.forName("US-ASCII"));
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(httpStatusCodeException);

        String response = usernameService.getUsernameByCardNumber(request);
        Error actual = xmlMapper.readValue(response, Error.class);

        assertEquals(String.valueOf(HttpStatus.OK), actual.getCode());
        assertEquals("Username cannot be found for card [8098791524808183]. Error: [400 null]", actual.getDescription());
    }

    private UsernameRequestCommand buildUsernameRequestCommand() {
        return UsernameRequestCommand.builder()
                .url("http://url")
                .locale("en-GB")
                .cardNumber(CARD_NUMBER)
                .headers(Headers.builder()
                        .casinoName(CASINO)
                        .xClientRequestId("abc")
                        .xForwardedFor("abc")
                        .xLcCid("abc")
                        .build())
                .build();
    }
}
