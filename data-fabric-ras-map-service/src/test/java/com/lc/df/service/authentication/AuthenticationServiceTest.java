package com.lc.df.service.authentication;

import com.lc.df.ras.Headers;
import com.lc.df.ras.authentication.request.AuthenticationRequest;
import com.lc.df.ras.authentication.request.AuthenticationRequestCommand;
import com.lc.df.ras.authentication.request.CardLogin;
import com.lc.df.ras.authentication.response.AuthenticationResponse;
import com.lc.df.service.TestBase;
import com.lc.df.service.utils.Utils;
import com.playtech.ims.LoginResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import static com.lc.df.service.utils.Constants.MESSAGE_AUTHENTICATED;
import static com.lc.df.service.utils.Constants.MESSAGE_NOT_AUTHENTICATED;
import static com.lc.df.service.utils.Constants.MESSAGE_NOT_REGISTRED_CARD;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceTest extends TestBase {

    private final String DEVICE_TYPE = "PC";
    private final String SHOP_CODE = "8008";
    private final String CARD_NUMBER = "9000000000428152";
    private final String CARD_PIN = "Nz9Nma+qUQfzsq2tuDGc8T4EFxaU18ZFiQCbVcfxlfCCeNYuzSgBIKyFjXrw6eT5xAYp0GpoZZRmyaW8djbSbqG+qQ+Njfj6OhCzpwLinJQkUTiwNVQgObWPXW\\/4lkS1hSsB+IsRHXzSAUZUmUxTwF\\/kRDTSwzkli1yN261BCEw=";

    @InjectMocks
    AuthenticationServiceImpl authenticationService;

    @Mock
    Utils utils;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void whenCustomerLoginCardNumberIsInvalidThenNotRegistredCardReturned() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_REGISTRED_CARD)
                .build();
        AuthenticationResponse actual = authenticationService.handleCustomerLoginAPIException(ex);
        testAuthenticationResponse(expected, actual);
    }

    @Test
    public void whenCustomerCardPinIsInvalidThenNotAuthenticatedCardReturned() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_AUTHENTICATED)
                .build();
        AuthenticationResponse actual = authenticationService.handleCustomerLoginAPIException(ex);
        testAuthenticationResponse(expected, actual);
    }

    @Test
    public void whenAnyHttpClientErrorExceptionThenNotAuthenticatedCardReturned() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_AUTHENTICATED)
                .build();
        AuthenticationResponse actual = authenticationService.handleCustomerLoginAPIException(ex);
        testAuthenticationResponse(expected, actual);
    }

    @Test
    public void whenAnyExceptionForHandleCustomerLoginAPIExceptionIsPassedThenNotAutheticatedMessageIsThrown() {
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_AUTHENTICATED)
                .build();
        AuthenticationResponse actual = authenticationService.handleCustomerLoginAPIException(new Exception());
        testAuthenticationResponse(expected, actual);
    }

    @Test
    public void whenCustomerCardLoginApiCallThrowsUnauthorizedStatusThenReturnCardNotRegistred() {
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_REGISTRED_CARD)
                .build();

        AuthenticationRequestCommand authenticationRequestCommand = AuthenticationRequestCommand.builder()
                .url("http://url")
                .locale("locale")
                .authenticationRequest(AuthenticationRequest.builder()
                        .deviceType(DEVICE_TYPE)
                        .shopCode(SHOP_CODE)
                        .cardLogin(CardLogin.builder()
                                .cardNumber(CARD_NUMBER)
                                .cardPIN(CARD_PIN)
                                .build())
                        .build())
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .casinoName("Casino")
                        .build())
                .build();

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));

        AuthenticationResponse actual = authenticationService.routeRASAuthenticationToDF(authenticationRequestCommand);
        testAuthenticationResponse(expected, actual);
    }

    @Test
    public void whenCustomerCardLoginApiCallThrowsAnyErrorCodeThenReturnNotAuthenticated() {
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_NOT_AUTHENTICATED)
                .build();

        AuthenticationRequestCommand authenticationRequestCommand = AuthenticationRequestCommand.builder()
                .url("http://url")
                .locale("locale")
                .authenticationRequest(AuthenticationRequest.builder()
                        .deviceType(DEVICE_TYPE)
                        .shopCode(SHOP_CODE)
                        .cardLogin(CardLogin.builder()
                                .cardNumber(CARD_NUMBER)
                                .cardPIN(CARD_PIN)
                                .build())
                        .build())
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .casinoName("Casino")
                        .build())
                .build();

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

        AuthenticationResponse actual = authenticationService.routeRASAuthenticationToDF(authenticationRequestCommand);
        testAuthenticationResponse(expected, actual);
    }

    @Test
    public void whenAuthenticationIsSuccessfullThenReturnSuccessMessage() {
        AuthenticationResponse expected = AuthenticationResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(0)
                .authenticationStatus(MESSAGE_AUTHENTICATED)
                .build();

        AuthenticationRequestCommand authenticationRequestCommand = AuthenticationRequestCommand.builder()
                .url("http://url")
                .locale("locale")
                .authenticationRequest(AuthenticationRequest.builder()
                        .deviceType(DEVICE_TYPE)
                        .shopCode(SHOP_CODE)
                        .cardLogin(CardLogin.builder()
                                .cardNumber(CARD_NUMBER)
                                .cardPIN(CARD_PIN)
                                .build())
                        .build())
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .casinoName("Casino")
                        .build())
                .build();

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(new LoginResponse());

        AuthenticationResponse actual = authenticationService.routeRASAuthenticationToDF(authenticationRequestCommand);
        testAuthenticationResponse(expected, actual);
    }

    private void testAuthenticationResponse(AuthenticationResponse expected, AuthenticationResponse actual) {
        assertEquals(expected.getResultMessage(), actual.getResultMessage());
        assertEquals(expected.getResultCode(), actual.getResultCode());
        assertEquals(expected.getAuthenticationStatus(), actual.getAuthenticationStatus());
    }

}
