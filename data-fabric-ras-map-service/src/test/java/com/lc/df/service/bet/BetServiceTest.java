package com.lc.df.service.bet;

import com.lc.df.ras.Headers;
import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;
import com.lc.df.ras.betting.request.BetRequest;
import com.lc.df.ras.betting.request.CancelBetRequest;
import com.lc.df.ras.betting.request.CancelBetRequestCommand;
import com.lc.df.ras.betting.request.CardDetails;
import com.lc.df.ras.betting.request.Money;
import com.lc.df.ras.betting.request.PlaceBetRequestCommand;
import com.lc.df.ras.betting.request.SettleBetRequest;
import com.lc.df.ras.betting.request.SettleBetRequestCommand;
import com.lc.df.ras.betting.request.ShopCode;
import com.lc.df.ras.betting.request.Slip;
import com.lc.df.ras.betting.request.UnsettleBetRequest;
import com.lc.df.ras.betting.request.UnsettleBetRequestCommand;
import com.lc.df.ras.betting.request.ims.Amount;
import com.lc.df.ras.betting.request.ims.ContextByClientParameters;
import com.lc.df.ras.betting.request.ims.ImsGeneralMoneyTransactionRequest;
import com.lc.df.ras.betting.request.ims.ParentTransactionReference;
import com.lc.df.ras.betting.response.BetPlacementResult;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.ras.betting.response.ims.ImsGeneralMoneyTransactionResponse;
import com.lc.df.ras.betting.response.ims.ImsWalletBatchResponse;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.TestBase;
import com.lc.df.service.balance.BalanceService;
import com.lc.df.service.exception.ImsErrorException;
import com.lc.df.service.username.UsernameService;
import com.lc.df.service.utils.Utils;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;

import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.CURRENCY_GBP;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_CANCEL_BET;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_PLACE_BET;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_SETTLE_BET_VOID;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_SETTLE_BET_WIN;
import static com.lc.df.service.utils.Constants.IMS_ACTION_TYPE_UNSETTLE_BET;
import static com.lc.df.service.utils.Constants.IMS_CLIENT_PLATFORM_RETAIL_OTC;
import static com.lc.df.service.utils.Constants.IMS_CLIENT_TYPE_RETAIL;
import static com.lc.df.service.utils.Constants.IMS_ERROR_CODE_MISSING_FOUNDS;
import static com.lc.df.service.utils.Constants.IMS_ERROR_CODE_USER_NOT_FOUND;
import static com.lc.df.service.utils.Constants.IMS_LANG_EN;
import static com.lc.df.service.utils.Constants.MESSAGE_INSUFFICIENT_FUNDS;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BetServiceTest extends TestBase {

    @InjectMocks
    BetServiceImpl betService;

    @Mock
    UsernameService usernameService;

    @Mock
    BalanceService balanceService;

    @Mock
    Utils utils;

    private static final String USERNAME = "imsusername";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenUsernameRetrivedByCardNumberExistsInIMSThenReturnIt() throws DataFabricException {
        GetIdTokenInfoResponse idTokenInfoResponse = new GetIdTokenInfoResponse();
        idTokenInfoResponse.setUsername(USERNAME);

        when(usernameService.getUsernameResponseByCardNumber(any(UsernameRequestCommand.class)))
                .thenReturn(idTokenInfoResponse);

        assertEquals(USERNAME, betService.getUsernameForPlaceBet(PlaceBetRequestCommand.placeBetRequestCommandBuilder().build()));
    }

    @Test
    public void whenUsernameRetrivedByCardNumberDoesntExistsInIMSThenThrowMissinUsernameException() {
        when(usernameService.getUsernameResponseByCardNumber(any(UsernameRequestCommand.class)))
                .thenReturn(null);

        try {
            betService.getUsernameForPlaceBet(PlaceBetRequestCommand.placeBetRequestCommandBuilder()
                    .betRequest(BetRequest.builder()
                            .cardLogin(CardDetails.builder()
                                    .cardNumber("1234567890")
                                    .build())
                            .build())
                    .build());
            fail("Username doesnt exist in IMS so that an exception should be thrown at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.MISSING_USERNAME, e.getCode());
        }
    }

    @Test
    public void whenPlaceBetToImsAndListOfGeneralMoneyTRansactionIsEmptyThenThrowSWRException() {
        try {
            betService.prepareImsBetWalletForPlaceBet(PlaceBetRequestCommand.placeBetRequestCommandBuilder().build(), USERNAME, Arrays.asList());
            fail("GeneralMoneytransaction list is empty so that an SWR exception should be thrown at the above line");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Expected a list of requests built from slips, but got 0 requests.", dfe.getDescription());
        }
    }

    @Test
    public void whenPlaceImsBetWalletIsSuccessThenReturnSuccessfulMessage() throws DataFabricException {

        String messageId = "MSG1";
        String errorCode = "0";

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                                .messageId(messageId)
                                .errorCode(errorCode)
                                .build());

        CallResult result = betService.prepareImsBetWalletForPlaceBet(buildBetRequestCommand(), USERNAME, moneyTransactionPlaceBetRequestList());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
        assertNull(result.getMissingFunds());
    }

    @Test
    public void whenPlaceImsWalletThrowsUnknownExCodeThenSWRExceptionIsThrown() {
        String imsErrorExCode = "IMS-123";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(imsErrorExCode, "IMS internal error"));

        try {
            betService.prepareImsBetWalletForPlaceBet(buildBetRequestCommand(), USERNAME, moneyTransactionPlaceBetRequestList());
            fail("IMS uncaught exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("IMS returned unexpected error code: [" + imsErrorExCode + "]", dfe.getDescription());
        }
    }

    @Test
    public void whenPlaceImsWalletThrowsUserNotFoundExCodeThenSWRExceptionIsThrown() {
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(IMS_ERROR_CODE_USER_NOT_FOUND, "IMS user not found."));

        try {
            betService.prepareImsBetWalletForPlaceBet(buildBetRequestCommand(), USERNAME, moneyTransactionPlaceBetRequestList());
            fail("IMS user not found exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Username [" + USERNAME + "] was not found in IMS", dfe.getDescription());
        }
    }

   /* @Test
    public void whenPlaceImsWalletThrowsMissingFoundsExCodeThenSendBackTheApproprieteResopnse() throws DataFabricException {
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenAnswer(answer(403));

        when(balanceService.getPlayerBalanceByUsername(any(UserBalanceCommand.class), anyString()))
                .thenReturn(BalanceResponse.builder()
                        .totalAccountBalance("200")
                        .build());

        CallResult result = betService.prepareImsBetWalletForPlaceBet(buildBetRequestCommand(), USERNAME, moneyTransactionPlaceBetRequestList());

        assertEquals(MESSAGE_INSUFFICIENT_FUNDS, result.getResultMessage());
        assertEquals("0", result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
        assertEquals("450", result.getMissingFunds().getPence());
    }*/

    private Answer answer(int httpStatus) {
        return (invocation) -> {
            if (httpStatus >= 403) {
                throw new RestClientException("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><error><code>UMS-108</code><description>Player has free credit=0.0 on accounts=[DEBIT_CARD_DEPOSITS_BALANCE, CREDIT_CARD_WINNINGS_BALANCE, CREDIT_CARD_DEPOSITS_BALANCE, MAIN_REAL_BALANCE]. Change amount=10.0</description><originally>UMS-114 Sublogic (GeneralMoneyTransactionLogic) finished with error: 108</originally><severity>ERROR</severity></error>");
            }
            return null;
        };
    }

    @Test
    public void whenPlaceBetSuccessfulThenReturnTheResponse() throws DataFabricException {

        GetIdTokenInfoResponse idTokenInfoResponse = new GetIdTokenInfoResponse();
        idTokenInfoResponse.setUsername(USERNAME);
        when(usernameService.getUsernameResponseByCardNumber(any(UsernameRequestCommand.class)))
                .thenReturn(idTokenInfoResponse);

        String messageId = "MSG1";
        String errorCode = "0";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());

        BetPlacementResult result = betService.placeBet(buildBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
        assertNull(result.getMissingFunds());
    }

    @Test
    public void whenPlaceBetSuccessfulButDifferentErrorCodeThanZeroThenReturnTheResponseAndTheCode() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        String imsErrorCode = "105";

        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse.setErrorCode(imsErrorCode);
        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse1 = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse1.setErrorCode(errorCode);

        GetIdTokenInfoResponse idTokenInfoResponse = new GetIdTokenInfoResponse();
        idTokenInfoResponse.setUsername(USERNAME);
        when(usernameService.getUsernameResponseByCardNumber(any(UsernameRequestCommand.class)))
                .thenReturn(idTokenInfoResponse);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .generalMoneyTransactionResponses(Arrays.asList(imsGeneralMoneyTransactionResponse, imsGeneralMoneyTransactionResponse1))
                        .build());

        CallResult result = betService.placeBet(buildBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(imsErrorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    @Test
    public void whenCancelImsBetWalletIsSuccessThenReturnSuccessfulMessage() throws DataFabricException {

        String messageId = "MSG1";
        String errorCode = "0";

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());

        CallResult result = betService.prepareImsBetWallet(buildCancelBetRequestCommand(), USERNAME, moneyTransactionPlaceBetRequestList());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
        assertNull(result.getMissingFunds());
    }

    @Test
    public void whenCancelBetThrowsUserNotFoundExCodeThenSWRExceptionIsThrown() {
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(IMS_ERROR_CODE_USER_NOT_FOUND, "IMS user not found."));

        try {
            betService.prepareImsBetWallet(buildCancelBetRequestCommand(), USERNAME, moneyTransactionCancelBetRequestList());
            fail("IMS user not found exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Username [" + USERNAME + "] was not found in IMS", dfe.getDescription());
        }
    }

    @Test
    public void whenCancelBetThrowsAnyImsErrorThenSWRExceptionIsThrown() {
        String imsErrorExCode = "IMS-123";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(imsErrorExCode, "IMS internal error"));

        try {
            betService.prepareImsBetWallet(buildCancelBetRequestCommand(), USERNAME, moneyTransactionCancelBetRequestList());
            fail("IMS uncaught exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("IMS returned unexpected error code: [" + imsErrorExCode + "]", dfe.getDescription());
        }
    }

    @Test
    public void whenCancelBetToImsAndListOfGeneralMoneyTRansactionIsEmptyThenThrowSWRException() {
        try {
            betService.prepareImsBetWallet(CancelBetRequestCommand.cancelBetRequestCommandBuilder().build(), USERNAME, Arrays.asList());
            fail("GeneralMoneytransaction list is empty so that an SWR exception should be thrown at the above line");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Expected a list of requests built from slips, but got 0 requests.", dfe.getDescription());
        }
    }

    @Test
    public void whenCancelBetSuccessfulButDifferentErrorCodeThanZeroThenReturnTheResponseAndTheCode() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        String imsErrorCode = "105";

        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse.setErrorCode(imsErrorCode);
        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse1 = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse1.setErrorCode(errorCode);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .generalMoneyTransactionResponses(Arrays.asList(imsGeneralMoneyTransactionResponse, imsGeneralMoneyTransactionResponse1))
                        .build());

        CallResult result = betService.cancelBet(buildCancelBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(imsErrorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    @Test
    public void whenCancelBetSuccessfulThenReturnTheResponse() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());

        CallResult result = betService.cancelBet(buildCancelBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    @Test
    public void whenSettleImsBetWalletIsSuccessThenReturnSuccessfulMessage() throws DataFabricException {

        String messageId = "MSG1";
        String errorCode = "0";

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());

        CallResult result = betService.prepareImsBetWallet(buildSettleBetRequestCommand(), USERNAME, moneyTransactionSettleBetRequestList());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
        assertNull(result.getMissingFunds());
    }

    @Test
    public void whenSettleBetThrowsUserNotFoundExCodeThenSWRExceptionIsThrown() {
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(IMS_ERROR_CODE_USER_NOT_FOUND, "IMS user not found."));

        try {
            betService.prepareImsBetWallet(buildSettleBetRequestCommand(), USERNAME, moneyTransactionSettleBetRequestList());
            fail("IMS user not found exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Username [" + USERNAME + "] was not found in IMS", dfe.getDescription());
        }
    }

    @Test
    public void whenSettleBetThrowsAnyImsErrorThenSWRExceptionIsThrown() {
        String imsErrorExCode = "IMS-123";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(imsErrorExCode, "IMS internal error"));

        try {
            betService.prepareImsBetWallet(buildSettleBetRequestCommand(), USERNAME, moneyTransactionSettleBetRequestList());
            fail("IMS uncaught exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("IMS returned unexpected error code: [" + imsErrorExCode + "]", dfe.getDescription());
        }
    }

    @Test
    public void whenSettleBetToImsAndListOfGeneralMoneyTransactionIsEmptyThenThrowSWRException() {
        try {
            betService.prepareImsBetWallet(SettleBetRequestCommand.settleBetRequestCommandBuilder().build(), USERNAME, Arrays.asList());
            fail("GeneralMoneytransaction list is empty so that an SWR exception should be thrown at the above line");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Expected a list of requests built from slips, but got 0 requests.", dfe.getDescription());
        }
    }

    @Test
    public void whenSettleBetSuccessfulThenReturnTheResponse() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());

        CallResult result = betService.settleBet(buildSettleBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    @Test
    public void whenSettleBetSuccessfulButDifferentErrorCodeThanZeroThenReturnTheResponseAndTheCode() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        String imsErrorCode = "105";

        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse.setErrorCode(imsErrorCode);
        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse1 = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse1.setErrorCode(errorCode);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .generalMoneyTransactionResponses(Arrays.asList(imsGeneralMoneyTransactionResponse, imsGeneralMoneyTransactionResponse1))
                        .build());

        CallResult result = betService.settleBet(buildSettleBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(imsErrorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    @Test
    public void whenUnsettleImsBetWalletIsSuccessThenReturnSuccessfulMessage() throws DataFabricException {

        String messageId = "MSG1";
        String errorCode = "0";

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());

        CallResult result = betService.prepareImsBetWallet(buildUnsettleBetRequestCommand(), USERNAME, moneyTransactionUnsettleBetRequestList());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
        assertNull(result.getMissingFunds());
    }

    @Test
    public void whenUnsettleBetThrowsUserNotFoundExCodeThenSWRExceptionIsThrown() {
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(IMS_ERROR_CODE_USER_NOT_FOUND, "IMS user not found."));

        try {
            betService.prepareImsBetWallet(buildUnsettleBetRequestCommand(), USERNAME, moneyTransactionUnsettleBetRequestList());
            fail("IMS user not found exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Username [" + USERNAME + "] was not found in IMS", dfe.getDescription());
        }
    }

    @Test
    public void whenUnsettleBetThrowsAnyImsErrorThenSWRExceptionIsThrown() {
        String imsErrorExCode = "IMS-123";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenThrow(new ImsErrorException(imsErrorExCode, "IMS internal error"));

        try {
            betService.prepareImsBetWallet(buildUnsettleBetRequestCommand(), USERNAME, moneyTransactionUnsettleBetRequestList());
            fail("IMS uncaught exception should be thrown at line above");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("IMS returned unexpected error code: [" + imsErrorExCode + "]", dfe.getDescription());
        }
    }

    @Test
    public void whenUnsettleBetToImsAndListOfGeneralMoneyTransactionIsEmptyThenThrowSWRException() {
        try {
            betService.prepareImsBetWallet(UnsettleBetRequestCommand.unsettleBetRequestCommandBuilder().build(), USERNAME, Arrays.asList());
            fail("GeneralMoneytransaction list is empty so that an SWR exception should be thrown at the above line");
        } catch (DataFabricException dfe) {
            assertEquals(DataFabricErrorCodes.SOMETHING_WENT_WRONG, dfe.getCode());
            assertEquals("Expected a list of requests built from slips, but got 0 requests.", dfe.getDescription());
        }
    }

    @Test
    public void whenUnsettleBetSuccessfulThenReturnTheResponse() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .build());
        when(utils.transformPenceToPounds(any(String.class)))
                .thenReturn("1");

        CallResult result = betService.unsettleBet(buildUnsettleBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(errorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    @Test
    public void whenUnsettleBetSuccessfulButDifferentErrorCodeThanZeroThenReturnTheResponseAndTheCode() throws DataFabricException {
        String messageId = "MSG1";
        String errorCode = "0";
        String imsErrorCode = "105";

        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse.setErrorCode(errorCode);
        ImsGeneralMoneyTransactionResponse imsGeneralMoneyTransactionResponse1 = new ImsGeneralMoneyTransactionResponse();
        imsGeneralMoneyTransactionResponse1.setErrorCode(imsErrorCode);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(ImsWalletBatchResponse.builder()
                        .messageId(messageId)
                        .errorCode(errorCode)
                        .generalMoneyTransactionResponses(Arrays.asList(imsGeneralMoneyTransactionResponse, imsGeneralMoneyTransactionResponse1))
                        .build());

        when(utils.transformPenceToPounds(any(String.class)))
                .thenReturn("1");

        CallResult result = betService.unsettleBet(buildUnsettleBetRequestCommand());

        assertEquals(MESSAGE_SUCCESS, result.getResultMessage());
        assertEquals(imsErrorCode, result.getResultCode());
        assertEquals(USERNAME, result.getUsername());
    }

    private PlaceBetRequestCommand buildBetRequestCommand() {
        return PlaceBetRequestCommand.placeBetRequestCommandBuilder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .betRequest(BetRequest.builder()
                        .shopCode(ShopCode.builder()
                                .code("1338")
                                .build())
                        .cardLogin(CardDetails.builder()
                                .cardNumber("12321341")
                                .cardPIN("1234")
                                .cardUID("2343243242")
                                .build())
                        .slips(Arrays.asList(
                                Slip.builder()
                                        .slipNumber("133813351675001")
                                        .amount(Money.builder()
                                                .pence("550")
                                                .build())
                                        .build(),
                                Slip.builder()
                                        .slipNumber("133813351675002")
                                        .amount(Money.builder()
                                                .pence("100")
                                                .build())
                                        .build()
                        ))
                        .build())
                .url("url")
                .build();
    }

    private CancelBetRequestCommand buildCancelBetRequestCommand() {
        return CancelBetRequestCommand.cancelBetRequestCommandBuilder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .cancelBetRequest(CancelBetRequest.builder()
                        .slip(Slip.builder()
                                .slipNumber("133813351675001")
                                .amount(Money.builder()
                                        .pence("550")
                                        .build())
                                .date("2018-09-04 09:19:34.318")
                                .description("desc")
                                .build())
                        .username(USERNAME)
                        .deviceType("PC")
                        .parentTransactionCode("321101008698001")
                        .parentTransactionDate("2018-07-05 08:45:52.000")
                        .build()
                )
                .url("url")
                .build();
    }

    private SettleBetRequestCommand buildSettleBetRequestCommand() {
        return SettleBetRequestCommand.settleBetRequestCommandBuilder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .settleBetRequest(SettleBetRequest.builder()
                        .slipNumber("133813351675001")
                        .username(USERNAME)
                        .winAmount(Money.builder()
                                .pence("550")
                                .build())
                        .voidAmount(Money.builder()
                                .pence("0")
                                .build())
                        .deviceType("PC")
                        .date("2018-07-05 08:05:10.000")
                        .parentTransactionCode("321101008698001")
                        .parentTransactionDate("2018-07-05 08:45:52.000")
                        .build()
                )
                .url("url")
                .build();
    }

    private UnsettleBetRequestCommand buildUnsettleBetRequestCommand() {
        return UnsettleBetRequestCommand.unsettleBetRequestCommandBuilder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .unsettleBetRequest(UnsettleBetRequest.builder()
                        .slipNumber("133813351675001")
                        .username(USERNAME)
                        .amount(Money.builder()
                                .pence("550")
                                .build())
                        .deviceType("PC")
                        .date("2018-07-05 08:05:10.000")
                        .parentTransactionCode("321101008698001")
                        .parentTransactionDate("2018-07-05 08:45:52.000")
                        .build()
                )
                .url("url")
                .build();
    }

    private List<ImsGeneralMoneyTransactionRequest> moneyTransactionPlaceBetRequestList() {
        return Arrays.asList(
                ImsGeneralMoneyTransactionRequest.builder()
                        .description("133813351675001")
                        .username(USERNAME)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                                .clientType(IMS_CLIENT_TYPE_RETAIL)
                                .deviceType("PC")
                                .languageCode(IMS_LANG_EN)
                                .venue("1338")
                                .build())
                        .transactionCode("133813351675001")
                        .transactionDate("2018-09-04 09:19:34.318")
                        .amount(Amount.builder()
                                .amount("550")
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                        .clientType(IMS_CLIENT_TYPE_RETAIL)
                        .actionType(IMS_ACTION_TYPE_PLACE_BET)
                        .build(),
                ImsGeneralMoneyTransactionRequest.builder()
                        .description("133813351675002")
                        .username(USERNAME)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                                .clientType(IMS_CLIENT_TYPE_RETAIL)
                                .deviceType("PC")
                                .languageCode(IMS_LANG_EN)
                                .venue("1338")
                                .build())
                        .transactionCode("133813351675002")
                        .transactionDate("2018-09-04 09:19:34.318")
                        .amount(Amount.builder()
                                .amount("100")
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                        .clientType(IMS_CLIENT_TYPE_RETAIL)
                        .actionType(IMS_ACTION_TYPE_PLACE_BET)
                        .build()
        );
    }

    private List<ImsGeneralMoneyTransactionRequest> moneyTransactionCancelBetRequestList() {
        return Arrays.asList(
                ImsGeneralMoneyTransactionRequest.builder()
                        .description("133813351675001")
                        .username(USERNAME)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                                .clientType(IMS_CLIENT_TYPE_RETAIL)
                                .deviceType("PC")
                                .languageCode(IMS_LANG_EN)
                                .venue("1338")
                                .build())
                        .transactionCode("133813351675001")
                        .transactionDate("2018-09-04 09:19:34.318")
                        .amount(Amount.builder()
                                .amount("550")
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                        .clientType(IMS_CLIENT_TYPE_RETAIL)
                        .actionType(IMS_ACTION_TYPE_CANCEL_BET)
                        .build()
        );
    }

    private List<ImsGeneralMoneyTransactionRequest> moneyTransactionSettleBetRequestList() {
        return Arrays.asList(
                ImsGeneralMoneyTransactionRequest.builder()
                        .username(USERNAME)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                                .clientType(IMS_CLIENT_TYPE_RETAIL)
                                .deviceType("PC")
                                .languageCode(IMS_LANG_EN)
                                .build())
                        .transactionCode("133813351675001")
                        .transactionDate("2018-09-04 09:19:34.318")
                        .amount(Amount.builder()
                                .amount("550")
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                        .clientType(IMS_CLIENT_TYPE_RETAIL)
                        .actionType(IMS_ACTION_TYPE_SETTLE_BET_WIN)
                        .parentTransactionReference(ParentTransactionReference.builder()
                                .transactionCode("133813351675001")
                                .transactionDate("2018-09-04 09:19:34.318")
                                .build())
                        .build(),
                ImsGeneralMoneyTransactionRequest.builder()
                        .username(USERNAME)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                                .clientType(IMS_CLIENT_TYPE_RETAIL)
                                .deviceType("PC")
                                .languageCode(IMS_LANG_EN)
                                .build())
                        .transactionCode("133813351675001")
                        .transactionDate("2018-09-04 09:19:34.318")
                        .amount(Amount.builder()
                                .amount("0")
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                        .clientType(IMS_CLIENT_TYPE_RETAIL)
                        .actionType(IMS_ACTION_TYPE_SETTLE_BET_VOID)
                        .parentTransactionReference(ParentTransactionReference.builder()
                                .transactionCode("133813351675001")
                                .transactionDate("2018-09-04 09:19:34.318")
                                .build())
                        .build()
        );
    }

    private List<ImsGeneralMoneyTransactionRequest> moneyTransactionUnsettleBetRequestList() {
        return Arrays.asList(
                ImsGeneralMoneyTransactionRequest.builder()
                        .description("133813351675001")
                        .username(USERNAME)
                        .contextByClientParameters(ContextByClientParameters.builder()
                                .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                                .clientType(IMS_CLIENT_TYPE_RETAIL)
                                .deviceType("PC")
                                .languageCode(IMS_LANG_EN)
                                .venue("1338")
                                .build())
                        .transactionCode("133813351675001")
                        .transactionDate("2018-09-04 09:19:34.318")
                        .amount(Amount.builder()
                                .amount("550")
                                .currencyCode(CURRENCY_GBP)
                                .build())
                        .clientPlatform(IMS_CLIENT_PLATFORM_RETAIL_OTC)
                        .clientType(IMS_CLIENT_TYPE_RETAIL)
                        .actionType(IMS_ACTION_TYPE_UNSETTLE_BET)
                        .build()
        );
    }
}
