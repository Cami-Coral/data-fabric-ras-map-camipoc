package com.lc.df.service.balance;

import com.lc.df.ras.Headers;
import com.lc.df.ras.balance.request.CardRequest;
import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.playtech.DynamicBalance;
import com.lc.df.playtech.GetDynamicBalancesRequest;
import com.lc.df.playtech.GetDynamicBalancesResponse;
import com.lc.df.service.TestBase;
import com.lc.df.service.username.UsernameService;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.ResourceAccessException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.lc.df.service.utils.Constants.CURRENCY_GBP;
import static com.lc.df.service.utils.Constants.IMS_BALANCE_OTC_GAMING;
import static com.lc.df.service.utils.Constants.IMS_BALANCE_OTC_WITHDRAWABLE;
import static com.lc.df.service.utils.Constants.IMS_BALANCE_RUNNING_PLAYABLE;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static com.lc.df.service.utils.Constants.NA;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BalanceServiceTest extends TestBase {

    @InjectMocks
    BalanceServiceImpl balanceService;

    @Mock
    UsernameService usernameService;

    private List<String> balanceTypes = Arrays.asList(IMS_BALANCE_RUNNING_PLAYABLE, IMS_BALANCE_OTC_GAMING, IMS_BALANCE_OTC_WITHDRAWABLE);
    private String TOTAL_ACC_BALANCE = "100";
    private String OTC_GAMING_BALANCE = "80";
    private String OTC_WITHDRW_BALANCE = "20";
    private String RESULT_CODE = "0";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenBalancesAreProvidedThenCreateTheGetDynamicBalanceRequest() {
        GetDynamicBalancesRequest dynamicBalancesRequest = balanceService.buildBalanceRequest();

        int filteredBalanceTypes = balanceTypes.stream()
                .filter(bt -> dynamicBalancesRequest.getBalanceTypes().contains(bt))
                .collect(Collectors.toList())
                .size();

        assertEquals(balanceTypes.size(), filteredBalanceTypes);
    }

    @Test
    public void whenMapOfBalancesIsProvidedAndABalanceExistsThenReturnTheBalanceValue() {
        Map<String, DynamicBalance> balanceMap = new HashMap<>();

        DynamicBalance db1 = new DynamicBalance();
        db1.setBalanceType(IMS_BALANCE_RUNNING_PLAYABLE);
        DynamicBalance.Balance balance1 = new DynamicBalance.Balance();
        balance1.setAmount(new BigDecimal(100));
        balance1.setCurrencyCode(CURRENCY_GBP);
        db1.setBalance(balance1);
        balanceMap.put(IMS_BALANCE_RUNNING_PLAYABLE, db1);

        DynamicBalance db2 = new DynamicBalance();
        db2.setBalanceType(IMS_BALANCE_OTC_GAMING);
        DynamicBalance.Balance balance2 = new DynamicBalance.Balance();
        balance2.setAmount(new BigDecimal(100));
        balance2.setCurrencyCode(CURRENCY_GBP);
        db2.setBalance(balance2);
        balanceMap.put(IMS_BALANCE_OTC_GAMING, db2);

        assertEquals(db1.getBalance().getAmount().toString(), balanceService.getBalanceFromMap(balanceMap, IMS_BALANCE_RUNNING_PLAYABLE));
    }

    @Test
    public void whenMapOfBalancesIsProvidedAndABalanceDoesNotExistsThenReturnNA() {
        Map<String, DynamicBalance> balanceMap = new HashMap<>();
        assertEquals(NA, balanceService.getBalanceFromMap(balanceMap, IMS_BALANCE_RUNNING_PLAYABLE));
    }

    @Test
    public void whenGetPLayeBalanceByUSernameExistsThenReturnTheBalanceForThatUsername() {
        BalanceResponse expected = BalanceResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(RESULT_CODE)
                .totalAccountBalance(TOTAL_ACC_BALANCE)
                .otcBettingBalance(OTC_GAMING_BALANCE)
                .otcWithdrawableBalance(OTC_WITHDRW_BALANCE)
                .build();

        UserBalanceCommand userBalanceCommand = UserBalanceCommand.builder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .cardRequest(CardRequest.builder()
                        .cardNumber("123456789")
                        .cardPIN("1234")
                        .build())
                .build();

        GetDynamicBalancesResponse dynamicBalancesResponse = new GetDynamicBalancesResponse();
        GetDynamicBalancesResponse.Balances balances = new GetDynamicBalancesResponse.Balances();
        dynamicBalancesResponse.setBalances(balances);

        DynamicBalance db1 = new DynamicBalance();
        db1.setBalanceType(IMS_BALANCE_RUNNING_PLAYABLE);
        DynamicBalance.Balance balance1 = new DynamicBalance.Balance();
        balance1.setAmount(new BigDecimal(TOTAL_ACC_BALANCE));
        balance1.setCurrencyCode(CURRENCY_GBP);
        db1.setBalance(balance1);
        dynamicBalancesResponse.getBalances().getDynamicBalance().add(db1);

        DynamicBalance db2 = new DynamicBalance();
        db2.setBalanceType(IMS_BALANCE_OTC_GAMING);
        DynamicBalance.Balance balance2 = new DynamicBalance.Balance();
        balance2.setAmount(new BigDecimal(OTC_GAMING_BALANCE));
        balance2.setCurrencyCode(CURRENCY_GBP);
        db2.setBalance(balance2);
        dynamicBalancesResponse.getBalances().getDynamicBalance().add(db2);

        DynamicBalance db3 = new DynamicBalance();
        db3.setBalanceType(IMS_BALANCE_OTC_WITHDRAWABLE);
        DynamicBalance.Balance balance3 = new DynamicBalance.Balance();
        balance3.setAmount(new BigDecimal(OTC_WITHDRW_BALANCE));
        balance3.setCurrencyCode(CURRENCY_GBP);
        db3.setBalance(balance3);
        dynamicBalancesResponse.getBalances().getDynamicBalance().add(db3);
        dynamicBalancesResponse.setErrorCode(BigInteger.ZERO);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(dynamicBalancesResponse);

        BalanceResponse actual = balanceService.getPlayerBalanceByUsername(userBalanceCommand, "user");

        assertEquals(expected.getResultMessage(), actual.getResultMessage());
        assertEquals(expected.getResultCode(), actual.getResultCode());
        assertEquals(expected.getTotalAccountBalance(), actual.getTotalAccountBalance());
        assertEquals(expected.getOtcBettingBalance(), actual.getOtcBettingBalance());
        assertEquals(expected.getOtcWithdrawableBalance(), actual.getOtcWithdrawableBalance());
    }

    @Test
    public void whenImsFailsThenReturnExceptionInMessage() {
        String errorMessage = "Some issues during request";

        UserBalanceCommand userBalanceCommand = UserBalanceCommand.builder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .cardRequest(CardRequest.builder()
                        .cardNumber("123456789")
                        .cardPIN("1234")
                        .build())
                .build();

        BalanceResponse expected = BalanceResponse.builder()
                .resultMessage(errorMessage)
                .build();

        when(usernameService.getUsernameResponseByCardNumber(any(UsernameRequestCommand.class)))
                .thenThrow(new ResourceAccessException(errorMessage));

        BalanceResponse actual = balanceService.getPlayerBalanceByCardNumber(userBalanceCommand);

        assertEquals("ResourceAccessException: ".concat(expected.getResultMessage()), actual.getResultMessage());
    }

    @Test
    public void whenPlayerBalanceByCardNumberSuccessfulThenReturnSuccessResponse() {

        UserBalanceCommand userBalanceCommand = UserBalanceCommand.builder()
                .appName("a")
                .headers(Headers.builder()
                        .xClientRequestId("123")
                        .xForwardedFor("123")
                        .xLcCid("123")
                        .build())
                .cardRequest(CardRequest.builder()
                        .cardNumber("123456789")
                        .cardPIN("1234")
                        .build())
                .build();

        BalanceResponse expected = BalanceResponse.builder()
                .resultMessage(MESSAGE_SUCCESS)
                .resultCode(RESULT_CODE)
                .totalAccountBalance(TOTAL_ACC_BALANCE)
                .otcBettingBalance(OTC_GAMING_BALANCE)
                .otcWithdrawableBalance(OTC_WITHDRW_BALANCE)
                .build();

        GetIdTokenInfoResponse idTokenInfoResponse = new GetIdTokenInfoResponse();
        idTokenInfoResponse.setUsername("username");

        GetDynamicBalancesResponse dynamicBalancesResponse = new GetDynamicBalancesResponse();
        GetDynamicBalancesResponse.Balances balances = new GetDynamicBalancesResponse.Balances();
        dynamicBalancesResponse.setBalances(balances);

        DynamicBalance db1 = new DynamicBalance();
        db1.setBalanceType(IMS_BALANCE_RUNNING_PLAYABLE);
        DynamicBalance.Balance balance1 = new DynamicBalance.Balance();
        balance1.setAmount(new BigDecimal(TOTAL_ACC_BALANCE));
        balance1.setCurrencyCode(CURRENCY_GBP);
        db1.setBalance(balance1);
        dynamicBalancesResponse.getBalances().getDynamicBalance().add(db1);

        DynamicBalance db2 = new DynamicBalance();
        db2.setBalanceType(IMS_BALANCE_OTC_GAMING);
        DynamicBalance.Balance balance2 = new DynamicBalance.Balance();
        balance2.setAmount(new BigDecimal(OTC_GAMING_BALANCE));
        balance2.setCurrencyCode(CURRENCY_GBP);
        db2.setBalance(balance2);
        dynamicBalancesResponse.getBalances().getDynamicBalance().add(db2);

        DynamicBalance db3 = new DynamicBalance();
        db3.setBalanceType(IMS_BALANCE_OTC_WITHDRAWABLE);
        DynamicBalance.Balance balance3 = new DynamicBalance.Balance();
        balance3.setAmount(new BigDecimal(OTC_WITHDRW_BALANCE));
        balance3.setCurrencyCode(CURRENCY_GBP);
        db3.setBalance(balance3);
        dynamicBalancesResponse.getBalances().getDynamicBalance().add(db3);
        dynamicBalancesResponse.setErrorCode(BigInteger.ZERO);

        when(restTemplate.postForObject(anyString(), anyObject(), anyObject()))
                .thenReturn(dynamicBalancesResponse);


        when(usernameService.getUsernameResponseByCardNumber(any(UsernameRequestCommand.class)))
                .thenReturn(idTokenInfoResponse);

        BalanceResponse actual = balanceService.getPlayerBalanceByCardNumber(userBalanceCommand);

        assertEquals(expected.getResultMessage(), actual.getResultMessage());
        assertEquals(expected.getResultCode(), actual.getResultCode());
        assertEquals(expected.getTotalAccountBalance(), actual.getTotalAccountBalance());
        assertEquals(expected.getOtcBettingBalance(), actual.getOtcBettingBalance());
        assertEquals(expected.getOtcWithdrawableBalance(), actual.getOtcWithdrawableBalance());
    }
}
