package com.lc.df.ras.map.validation;

import ch.qos.logback.classic.Level;
import com.lc.df.ras.betting.request.BetRequest;
import com.lc.df.ras.betting.request.CancelBetRequest;
import com.lc.df.ras.betting.request.Money;
import com.lc.df.ras.betting.request.SettleBetRequest;
import com.lc.df.ras.betting.request.ShopCode;
import com.lc.df.ras.betting.request.Slip;
import com.lc.df.ras.betting.request.UnsettleBetRequest;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.logging.DataFabricLogger;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.lc.df.service.utils.Constants.DF;

public class RasMapValidation {

    public static final String DEVICE_TYPE_IS_MISSING = "Device type is missing.";
    public static final String DATE_FORMAT = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{3}";

    @Value("${module.name}")
    private String appName;

    @Autowired
    DataFabricLogger log;

    public void validateCardNumber(String cardNumber) throws DataFabricException {
        if (StringUtils.isBlank(cardNumber)) {
            log.log(appName, Level.ERROR, "Empty card-number provided", DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_CARD_NUMBER, "", DF);
        }
    }

    public void validateBetRequest(BetRequest betRequest) throws DataFabricException {
        checkDeviceTypeIsValid(betRequest.getDeviceType());

        boolean hasShoppingCode = Optional.ofNullable(betRequest.getShopCode())
                .map(ShopCode::getCode)
                .isPresent();

        if (!hasShoppingCode) {
            log.log(appName, Level.ERROR, "Shop code is missing.", DF);
            throw new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, "Shop code is missing.", DF);
        }

        validateSlips(betRequest);
    }

    public CallResult validateBet(BetRequest betRequest) {
        try {
            validateBetRequest(betRequest);
        } catch (DataFabricException dfe) {
            return CallResult.builder()
                    .resultMessage(dfe.getDescription())
                    .resultCode(dfe.getCode())
                    .build();
        }
        return null;
    }

    public void validateCancelBetRequest(CancelBetRequest cancelBetRequest) throws DataFabricException {
        checkUsernameIsValid(cancelBetRequest.getUsername());
        checkDeviceTypeIsValid(cancelBetRequest.getDeviceType());
        checkParentTransCodeAndDateAreValid(cancelBetRequest.getParentTransactionCode(), cancelBetRequest.getParentTransactionDate());
        validateSlip(cancelBetRequest.getSlip());
    }

    public CallResult validateCancelBet(CancelBetRequest cancelBetRequest) {
        try {
            validateCancelBetRequest(cancelBetRequest);
        } catch (DataFabricException dfe) {
            return CallResult.builder()
                    .resultMessage(dfe.getDescription())
                    .resultCode(dfe.getCode())
                    .build();
        }
        return null;
    }

    public void validateSettleBetRequest(SettleBetRequest settleBetRequest) throws DataFabricException {
        checkSlipNumberIsValid(settleBetRequest.getSlipNumber());
        checkUsernameIsValid(settleBetRequest.getUsername());

        if (StringUtils.isBlank(settleBetRequest.getDate()) || !settleBetRequest.getDate().matches(DATE_FORMAT)) {
            String errorMessage = String.format("Date [%s] is not a valid format", settleBetRequest.getDate());
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }

        checkDeviceTypeIsValid(settleBetRequest.getDeviceType());
        checkAmountsAreValid(settleBetRequest.getWinAmount(), settleBetRequest.getVoidAmount());
        checkParentTransCodeAndDateAreValid(settleBetRequest.getParentTransactionCode(), settleBetRequest.getParentTransactionDate());
    }

    public CallResult validateSettleBet(SettleBetRequest settleBetRequest) {
        try {
            validateSettleBetRequest(settleBetRequest);
        } catch (DataFabricException dfe) {
            return CallResult.builder()
                    .resultMessage(dfe.getDescription())
                    .resultCode(dfe.getCode())
                    .build();
        }
        return null;
    }

    public void validateUnsettleBetRequest(UnsettleBetRequest unsettleBetRequest) throws DataFabricException {
        checkUsernameIsValid(unsettleBetRequest.getUsername());
        // For resettlement Ras sends unique alpha-numeric string in here so that we need to relax the validation
        //checkSlipNumberIsValid(unsettleBetRequest.getSlipNumber());
        if (StringUtils.isBlank(unsettleBetRequest.getSlipNumber())) {
            String errorMessage = "Slip number is missing from the request";
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }

        if (StringUtils.isBlank(unsettleBetRequest.getDate())) {
            String errorMessage = "Date field is missing";
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, errorMessage, DF);
        }

        if (!unsettleBetRequest.getDate().matches(DATE_FORMAT)) {
            String errorMessage = String.format("Date [%s] is not a valid format", unsettleBetRequest.getDate());
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }

        String penceAsString = Optional.ofNullable(unsettleBetRequest.getAmount())
                .map(Money::getPence)
                .orElseThrow(() -> new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, "Amount field is missing", DF));
        int pence = getAmountValue(penceAsString, String.format("Slip[%s]", unsettleBetRequest.getSlipNumber()));
        if (pence < 0) {
            String errorMessage = String.format("Slip[%s] has a negative value for the amount[%s].", unsettleBetRequest.getSlipNumber(), penceAsString);
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }

        checkDeviceTypeIsValid(unsettleBetRequest.getDeviceType());
        checkParentTransCodeAndDateAreValid(unsettleBetRequest.getParentTransactionCode(), unsettleBetRequest.getParentTransactionDate());
    }

    public CallResult validateUnsettleBet(UnsettleBetRequest unsettleBetRequestt) {
        try {
            validateUnsettleBetRequest(unsettleBetRequestt);
        } catch (DataFabricException dfe) {
            return CallResult.builder()
                    .resultMessage(dfe.getDescription())
                    .resultCode(dfe.getCode())
                    .build();
        }
        return null;
    }

    private void validateSlips(BetRequest betRequest) throws DataFabricException {
        Map<String, Slip> slips = betRequest.getSlips().stream()
                .collect(Collectors.toMap(Slip::getSlipNumber, Function.identity()));

        for (Map.Entry<String, Slip> entry : slips.entrySet()) {
            Slip slip = entry.getValue();
            validateSlip(slip);
        }
    }

    private void validateSlip(Slip slip) throws DataFabricException {
        String errorMessage;
        checkSlipNumberIsValid(slip.getSlipNumber());

        if (StringUtils.isBlank(slip.getDate()) || !slip.getDate().matches(DATE_FORMAT)) {
            errorMessage = String.format("Slip date [%s] is not a valid format", slip.getDate());
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }

        String penceAsString = Optional.ofNullable(slip)
                .map(Slip::getAmount)
                .map(Money::getPence)
                .orElse("N/A");

        int pence = getAmountValue(penceAsString, String.format("Slip[%s]", slip.getSlipNumber()));
        if (pence < 0) {
            errorMessage = String.format("Slip[%s] has a negative value for the amount[%s].", slip.getSlipNumber(), penceAsString);
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }
    }

    private void checkSlipNumberIsValid(String slipNumber) throws DataFabricException {
        if (StringUtils.isBlank(slipNumber) || !slipNumber.matches("[0-9]+")) {
            String errorMessage = String.format("Slip number [%s] has an invalid format", slipNumber);
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }
    }

    private void checkUsernameIsValid(String username) throws DataFabricException {
        if (StringUtils.isBlank(username)) {
            log.log(appName, Level.ERROR, "Username is missing.", DF);
            throw new DataFabricException(DataFabricErrorCodes.MISSING_USERNAME, "Username is missing.", DF);
        }
    }

    private void checkDeviceTypeIsValid(String deviceType) throws DataFabricException {
        if (StringUtils.isBlank(deviceType)) {
            log.log(appName, Level.ERROR, DEVICE_TYPE_IS_MISSING, DF);
            throw new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, DEVICE_TYPE_IS_MISSING, DF);
        }
    }

    private void checkAmountsAreValid(Money winAmount, Money voidAmount) throws DataFabricException {
        String errorMsg;
        if (winAmount == null) {
            errorMsg = "[winAmount] field is missing";
            log.log(appName, Level.ERROR, errorMsg, DF);
            throw new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, errorMsg, DF);
        }

        if (voidAmount == null) {
            errorMsg = "[voidAmount] field is missing";
            log.log(appName, Level.ERROR, errorMsg, DF);
            throw new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, errorMsg, DF);
        }

        int winAmountValue = getAmountValue(winAmount.getPence(), "winAmount");
        int voidAmountValue = getAmountValue(voidAmount.getPence(), "voidAmount");

        if (winAmountValue < 0 || voidAmountValue < 0) {
            errorMsg = "[winAmount] or [voidAmount] field has a negative value";
            log.log(appName, Level.ERROR, errorMsg, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMsg, DF);
        }

        if (winAmountValue == 0 && voidAmountValue == 0) {
            errorMsg = "At least one value for [winAmount] or [voidAmount] should be grater than 0";
            log.log(appName, Level.ERROR, errorMsg, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMsg, DF);
        }
    }

    private int getAmountValue(String value, String fieldName) throws DataFabricException {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            String errorMsg = String.format("[%s] doesn't have a valid value [%s]", fieldName, value);
            log.log(appName, Level.ERROR, errorMsg, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMsg, DF);
        }
    }

    private void checkParentTransCodeAndDateAreValid(String code, String date) throws DataFabricException {
        if (StringUtils.isBlank(code) ||
                StringUtils.isBlank(date)) {
            log.log(appName, Level.ERROR, "Parent transaction code or parent transaction date are missing.", DF);
            throw new DataFabricException(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, "Parent transaction code or parent transaction date are missing.", DF);
        }

        if (!date.matches(DATE_FORMAT)) {
            String errorMessage = String.format("Parent transaction date [%s] is not a valid format", date);
            log.log(appName, Level.ERROR, errorMessage, DF);
            throw new DataFabricException(DataFabricErrorCodes.INVALID_REQUEST, errorMessage, DF);
        }
    }
}
