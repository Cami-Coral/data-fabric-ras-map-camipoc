package com.lc.df.ras.map;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication(scanBasePackages = { "com.lc.df" })
public class RasMapApplication {

    public static void main(String[] args) {
        loadConfiguration();
        SpringApplication.run(RasMapApplication.class, args);
    }

    private static void loadConfiguration() {
        HashMap<String, String> settings = new HashMap<>();
        settings.put("IMS_URL", "ims.url.root");
        settings.put("IMS_CASINO_NAME", "ims.header.casinoName");
        settings.put("IMS_CERTIFICATE", "ims.ssl.keystore");
        settings.put("IMS_PASSWORD", "ims.ssl.password");
        settings.put("CUST_MODULE_ENV", "module.env");

        settings.forEach((key, value) -> {
            String item = System.getenv(key);
            if (item != null) {
                System.setProperty(settings.get(key), item);
            }
        });
    }

}
