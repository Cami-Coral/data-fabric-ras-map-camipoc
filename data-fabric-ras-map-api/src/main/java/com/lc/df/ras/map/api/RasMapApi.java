package com.lc.df.ras.map.api;

import com.lc.df.ras.Headers;
import com.lc.df.ras.authentication.request.AuthenticationRequest;
import com.lc.df.ras.authentication.request.AuthenticationRequestCommand;
import com.lc.df.ras.authentication.response.AuthenticationResponse;
import com.lc.df.ras.balance.request.CardRequest;
import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;
import com.lc.df.ras.betting.request.BetRequest;
import com.lc.df.ras.betting.request.CancelBetRequest;
import com.lc.df.ras.betting.request.CancelBetRequestCommand;
import com.lc.df.ras.betting.request.PlaceBetRequestCommand;
import com.lc.df.ras.betting.request.SettleBetRequest;
import com.lc.df.ras.betting.request.SettleBetRequestCommand;
import com.lc.df.ras.betting.request.UnsettleBetRequest;
import com.lc.df.ras.betting.request.UnsettleBetRequestCommand;
import com.lc.df.ras.betting.response.BetPlacementResult;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.ras.map.validation.RasMapValidation;
import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.service.authentication.AuthenticationService;
import com.lc.df.service.balance.BalanceService;
import com.lc.df.service.bet.BetService;
import com.lc.df.service.username.UsernameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.LOCALE_EN_GB;
import static com.lc.df.service.utils.Constants.PARAM_CARD_NUMBER;
import static com.lc.df.service.utils.Constants.PARAM_LOCALE;
import static com.lc.df.service.utils.Constants.X_CLIENT_REQUEST_ID;
import static com.lc.df.service.utils.Constants.X_FORWARDED_FOR;
import static com.lc.df.service.utils.Constants.X_LC_CID;


@Api(value = "RAS map API")

@Validated
@RestController
@RequestMapping(API_ROOT)
public class RasMapApi {

    @Value("${module.name}")
    private String appName;

    @Value("${ims.url.root}")
    private String urlImsRoot;

    @Value("${ims.url.login}")
    private String urlImsLogin;

    @Value("${ims.url.getidtoken}")
    private String urlImsGetIdToken;

    @Value("${ims.url.user.balance}")
    private String urlImsUserBalance;

    @Value("${ims.url.wallet.batch}")
    private String urlImsWalletBatch;

    @Value("${ims.header.casinoName}")
    private String casinoName;

    @Autowired
    RasMapValidation rasMapValidation;

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    BalanceService balanceService;

    @Autowired
    BetService betService;

    @Autowired
    UsernameService usernameService;

    @ApiOperation(value = "Redirect RAS requests for authentication", response = AuthenticationResponse.class)
    @PostMapping(value = "/authenticateAccount", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<AuthenticationResponse> authenticateAccount(@RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                      @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                      @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                      @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId,
                      @RequestBody AuthenticationRequest authenticationRequest) {

        AuthenticationRequestCommand authenticationRequestCommand = AuthenticationRequestCommand.builder()
                .appName(appName)
                .authenticationRequest(authenticationRequest)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .locale(locale)
                .url(urlImsRoot.concat(urlImsLogin))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(authenticationService.routeRASAuthenticationToDF(authenticationRequestCommand));
    }

    @ApiOperation(value = "Get user balance based on card number", response = BalanceResponse.class)
    @PostMapping(value = "/getPlayerBalances", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BalanceResponse> getPlayerBalances(@RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                      @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                      @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                      @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId,
                      @RequestBody CardRequest cardRequest) {

        UserBalanceCommand userBalanceCommand = UserBalanceCommand.builder()
                .appName(appName)
                .cardRequest(cardRequest)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .locale(locale)
                .imsTokenInfoUrl(urlImsRoot.concat(urlImsGetIdToken))
                .url(urlImsRoot.concat(urlImsUserBalance))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(balanceService.getPlayerBalanceByCardNumber(userBalanceCommand));
    }

    @ApiOperation(value = "Get username based on id token info (card number).", response = String.class)
    @GetMapping(value = "/imsapis/getusername.php", produces = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<String> getUsernameForCardNumber(@RequestParam(value = PARAM_CARD_NUMBER) String cardNumber,
                      @RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                      @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                      @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                      @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId) throws DataFabricException {

        rasMapValidation.validateCardNumber(cardNumber);

        UsernameRequestCommand usernameRequestCommand = UsernameRequestCommand.builder()
                .appName(appName)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .cardNumber(cardNumber)
                .locale(locale)
                .url(urlImsRoot.concat(urlImsGetIdToken))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_XML)
                .body(usernameService.getUsernameByCardNumber(usernameRequestCommand));
    }

    @ApiOperation(value = "Place a bet", response = CallResult.class)
    @PostMapping(value = "/bet", produces = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<BetPlacementResult> placeBet(@RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                                                       @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                                                       @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                                                       @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId,
                                                       @RequestBody BetRequest betRequest) throws DataFabricException {

        ResponseEntity errors = validateRequest(rasMapValidation.validateBet(betRequest));
        if (errors != null) {
            return errors;
        }

        PlaceBetRequestCommand betRequestCommand = PlaceBetRequestCommand.placeBetRequestCommandBuilder()
                .appName(appName)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .betRequest(betRequest)
                .locale(locale)
                .url(urlImsRoot.concat(urlImsWalletBatch))
                .urlImsUsername(urlImsRoot.concat(urlImsGetIdToken))
                .urlImsUserBalance(urlImsRoot.concat(urlImsUserBalance))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_XML)
                .body(betService.placeBet(betRequestCommand));
    }

    @ApiOperation(value = "Cancel a bet", response = CallResult.class)
    @PostMapping(value = "/cancelBet", produces = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<CallResult> cancelBet(@RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                                                @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                                                @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                                                @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId,
                                                @RequestBody CancelBetRequest cancelBetRequest) throws DataFabricException {

        ResponseEntity errors = validateRequest(rasMapValidation.validateCancelBet(cancelBetRequest));
        if (errors != null) {
            return errors;
        }

        CancelBetRequestCommand cancelBetRequestCommand = CancelBetRequestCommand.cancelBetRequestCommandBuilder()
                .appName(appName)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .cancelBetRequest(cancelBetRequest)
                .locale(locale)
                .url(urlImsRoot.concat(urlImsWalletBatch))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_XML)
                .body(betService.cancelBet(cancelBetRequestCommand));
    }

    @ApiOperation(value = "Settle a bet", response = CallResult.class)
    @PostMapping(value = "/settleBet", produces = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<CallResult> settleBet(@RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                                                @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                                                @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                                                @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId,
                                                @RequestBody SettleBetRequest settleBetRequest) throws DataFabricException {

        ResponseEntity errors = validateRequest(rasMapValidation.validateSettleBet(settleBetRequest));
        if (errors != null) {
            return errors;
        }

        SettleBetRequestCommand settleBetRequestCommand = SettleBetRequestCommand.settleBetRequestCommandBuilder()
                .appName(appName)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .settleBetRequest(settleBetRequest)
                .locale(locale)
                .url(urlImsRoot.concat(urlImsWalletBatch))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_XML)
                .body(betService.settleBet(settleBetRequestCommand));
    }

    @ApiOperation(value = "Unsettle a bet", response = CallResult.class)
    @PostMapping(value = "/unsettleBet", produces = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<CallResult> unsettleBet(@RequestParam(value = PARAM_LOCALE, required = false, defaultValue = LOCALE_EN_GB) String locale,
                                                  @RequestHeader(value = X_CLIENT_REQUEST_ID, required = false) String xClientRequestId,
                                                  @RequestHeader(value = X_FORWARDED_FOR, required = false) String xForwardedFor,
                                                  @RequestHeader(value = X_LC_CID, required = false) String xCorrelationId,
                                                  @RequestBody UnsettleBetRequest unsettleBetRequest) throws DataFabricException {

        ResponseEntity errors = validateRequest(rasMapValidation.validateUnsettleBet(unsettleBetRequest));
        if (errors != null) {
            return errors;
        }

        UnsettleBetRequestCommand unsettleBetRequestCommand = UnsettleBetRequestCommand.unsettleBetRequestCommandBuilder()
                .appName(appName)
                .headers(Headers.builder()
                        .casinoName(casinoName)
                        .xClientRequestId(Optional.ofNullable(xClientRequestId).orElse(UUID.randomUUID().toString()))
                        .xForwardedFor(Optional.ofNullable(xForwardedFor).orElse(UUID.randomUUID().toString()))
                        .xLcCid(Optional.ofNullable(xCorrelationId).orElse(UUID.randomUUID().toString()))
                        .build())
                .unsettleBetRequest(unsettleBetRequest)
                .locale(locale)
                .url(urlImsRoot.concat(urlImsWalletBatch))
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_XML)
                .body(betService.unsettleBet(unsettleBetRequestCommand));
    }

    private ResponseEntity validateRequest(CallResult betResult) {
        return Optional.ofNullable(betResult)
                .map(betError -> ResponseEntity.status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_XML)
                        .body(betError))
                .orElse(null);
    }

}
