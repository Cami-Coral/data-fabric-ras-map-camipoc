package com.lc.df.ras.map;

import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lc.df.ras.map.validation.RasMapValidation;
import com.lc.df.logging.DataFabricLogger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import static com.lc.df.service.utils.Constants.DF;

@Configuration
public class ApplicationConfig {

    @Value("${module.name}")
    private String appName;

    @Value("${ims.ssl.password}")
    private String imsSSLPassword;

    @Value("${ims.ssl.keystore}")
    private String imsSSLKeyStoreFileName;

    @Value("${ims.request.timeout}")
    private int imsRequestTimeout;

    @Value("${ims.response.timeout}")
    private int imsResponseTimeout;

    @Autowired
    DataFabricLogger log;


    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate imsSSLRestTemplate() {
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory(true));
        enforceXMLMarshalling(restTemplate);
        return restTemplate;
    }

    @Bean
    public RasMapValidation rasMapValidation() {
        return  new RasMapValidation();
    }

    @Bean
    public XmlMapper xmlMapper() {
        return new XmlMapper();
    }

    private void enforceXMLMarshalling(RestTemplate restTemplate) {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        Jaxb2RootElementHttpMessageConverter jaxbMessageConverter = new Jaxb2RootElementHttpMessageConverter();
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.APPLICATION_XML);
        jaxbMessageConverter.setSupportedMediaTypes(mediaTypes);
        messageConverters.add(jaxbMessageConverter);
        restTemplate.setMessageConverters(messageConverters);
    }

    private ClientHttpRequestFactory clientHttpRequestFactory(boolean isSSL) {

        HttpComponentsClientHttpRequestFactory factory;

        if (isSSL) {
            factory = new HttpComponentsClientHttpRequestFactory(getSSLHttpClient(imsSSLPassword, imsSSLKeyStoreFileName));
        } else {
            factory = new HttpComponentsClientHttpRequestFactory();
        }

        factory.setReadTimeout(imsRequestTimeout);
        factory.setConnectTimeout(imsResponseTimeout);

        return factory;
    }

    private HttpClient getSSLHttpClient(String pass, String keyStoreFileName) {
        SSLConnectionSocketFactory socketFactory = null;
        try {
            SSLContextBuilder sslContextBuilder = new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy());

            if (StringUtils.isNotBlank(pass) && StringUtils.isNotBlank(keyStoreFileName)) {
                char[] password = pass.toCharArray();
                sslContextBuilder.loadKeyMaterial(keyStore(keyStoreFileName, password), password);
            }

            socketFactory = new SSLConnectionSocketFactory(sslContextBuilder.build(),
                    new String[]{"TLSv1.2", "TLSv1.1", "TLSv1"}, null,
                    SSLConnectionSocketFactory.getDefaultHostnameVerifier());
        } catch (Exception e) {
            log.log(appName, Level.ERROR, String.format("[getSslHttpClient] - SSL connectivity issue: [%s]",
                    ExceptionUtils.getStackTrace(e)), DF);
        }

        return HttpClients.custom().setSSLSocketFactory(socketFactory).build();
    }

    private KeyStore keyStore(String file, char[] password) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream in = classloader.getResourceAsStream(file);
        if (in == null) {
            log.log(appName, Level.INFO, String.format("Certificate [%s] - [%s] cannot be found!", keyStore, keyStore.getType()), DF);
        }
        keyStore.load(in, password);

        return keyStore;
    }

}
