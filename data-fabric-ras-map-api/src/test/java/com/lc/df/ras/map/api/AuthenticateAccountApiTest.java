package com.lc.df.ras.map.api;

import com.lc.df.ras.authentication.request.AuthenticationRequestCommand;
import com.lc.df.ras.authentication.response.AuthenticationResponse;
import com.lc.df.service.authentication.AuthenticationService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.PARAM_API_KEY;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticateAccountApiTest extends TestBase {

    private final String RAS_AUTHETICATION_REQUEST = API_ROOT.concat("/authenticateAccount");
    List<String> mandatoryHeaders = Arrays.asList();

    @MockBean
    AuthenticationService authenticationService;

    @Test
    public void whenRASSendsAuthenticationRequestThenSendBackAuthenticationResponseFromDF() throws Exception {
        String rasAuthReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasAuthRequest.json"), "UTF-8");
        String rasRespAuth = IOUtils.toString(this.getClass().getResourceAsStream("/rasAuthRespAuthenticated.json"), "UTF-8");
        AuthenticationResponse authenticationResponse = objectMapper.readValue(rasRespAuth, AuthenticationResponse.class);

        doReturn(authenticationResponse).when(authenticationService).routeRASAuthenticationToDF(any(AuthenticationRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_AUTHETICATION_REQUEST)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rasAuthReq)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JSONAssert.assertEquals(rasRespAuth, result.getResponse().getContentAsString(), false);
    }




    @Test
    public void testMandatoryHeaders() {

        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        String rasAuthReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasAuthRequest.json"), "UTF-8");
        String rasRespAuth = IOUtils.toString(this.getClass().getResourceAsStream("/rasAuthRespAuthenticated.json"), "UTF-8");
        AuthenticationResponse authenticationResponse = objectMapper.readValue(rasRespAuth, AuthenticationResponse.class);

        doReturn(authenticationResponse).when(authenticationService).routeRASAuthenticationToDF(any(AuthenticationRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_AUTHETICATION_REQUEST)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rasAuthReq)
                .param(PARAM_API_KEY, "abc")
                .headers(headers);

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        Assert.assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }
}
