package com.lc.df.ras.map.api;

import com.lc.df.ras.betting.request.SettleBetRequestCommand;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.bet.BetService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.PARAM_API_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SettleBetApiTest extends TestBase {

    private final String RAS_SETTLE_BET_REQUEST = API_ROOT.concat("/settleBet");
    List<String> mandatoryHeaders = Arrays.asList();

    private String validSettleBetReq = null;
    private String validBetResp = null;

    @Before
    public void setup() throws IOException {
        validSettleBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestValid.xml"), "UTF-8");
        validBetResp = IOUtils.toString(this.getClass().getResourceAsStream("/rasBetResponseValid.xml"), "UTF-8");
    }

    @MockBean
    BetService betService;


    @Test
    public void whenSlipNumberIsInvalidThenThrowInvalidRequestException() throws Exception {
        String invalidSlipNoSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestInvalidSlipNo.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidSlipNoSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Slip number [1338133AQ51675001] has an invalid format</resultMessage>"));
    }

    @Test
    public void whenSlipNumberIsMissingThenThrowInvalidRequestException() throws Exception {
        String missingSlipNoSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingSlipNo.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingSlipNoSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Slip number [null] has an invalid format</resultMessage>"));
    }

    @Test
    public void whenUsernameIsNotPresentThenThrowMissingUsernameException() throws Exception {
        String missingUsernameSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingUsername.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingUsernameSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.MISSING_USERNAME + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Username is missing.</resultMessage>"));
    }

    @Test
    public void whenDateIsInvalidThenThrowInvalidRequestException() throws Exception {
        String invalidDateSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestInvalidDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidDateSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Date [2018-017-05 08:05:10.000] is not a valid format</resultMessage>"));
    }

    @Test
    public void whenDateIsMissingThenThrowInvalidRequestException() throws Exception {
        String missingDateSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingDateSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Date [null] is not a valid format</resultMessage>"));
    }

    @Test
    public void whenDeviceTypeIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingDeviceTypeSettleBetRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingDeviceType.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingDeviceTypeSettleBetRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Device type is missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransCodeIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingParentTransCodeSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingTransParentCode.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingParentTransCodeSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction code or parent transaction date are missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransDateIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingParentTransDateSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingParentTransDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingParentTransDateSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction code or parent transaction date are missing.</resultMessage>"));
    }

    @Test
    public void whenWinAmountIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingWinAmountSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingWinAmount.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingWinAmountSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>[winAmount] field is missing</resultMessage>"));
    }

    @Test
    public void whenVoidAmountIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingVoidAmountSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestMissingVoidAmount.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingVoidAmountSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>[voidAmount] field is missing</resultMessage>"));
    }

    @Test
    public void whenWinAmountIsNegativeThenThrowInvalidRequestException() throws Exception {
        String negativeWinAmountsSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestWinAmountsWithNegativeValue.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(negativeWinAmountsSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>[winAmount] or [voidAmount] field has a negative value</resultMessage>"));
    }

    @Test
    public void whenVoidAmountIsNegativeThenThrowInvalidRequestException() throws Exception {
        String negativeVoidAmountsSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestVoidAmountsWithNegativeValue.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(negativeVoidAmountsSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>[winAmount] or [voidAmount] field has a negative value</resultMessage>"));
    }

    @Test
    public void whenWinAmountIsZeroThenThrowInvalidRequestException() throws Exception {
        String zeroAmountsSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestZeroAmounts.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(zeroAmountsSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>At least one value for [winAmount] or [voidAmount] should be grater than 0</resultMessage>"));
    }


    @Test
    public void whenValidSettleBetRequestIsSuccessfulThenReturnSuccessResult() throws Exception {
        String validSettleBetRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasSettleBetRequestValid.xml"), "UTF-8");
        CallResult expectedResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(expectedResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validSettleBetRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        CallResult actualResponse = xmlMapper.readValue(result.getResponse().getContentAsString(), CallResult.class);

        assertEquals(expectedResponse.getResultMessage(), actualResponse.getResultMessage());
        assertEquals(expectedResponse.getResultCode(), actualResponse.getResultCode());
        assertEquals(expectedResponse.getUsername(), actualResponse.getUsername());
        assertEquals(expectedResponse.getMissingFunds(), actualResponse.getMissingFunds());
    }

    @Test
    public void testMandatoryHeaders() {
        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).settleBet(any(SettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_SETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validSettleBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(headers);

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        Assert.assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }
}
