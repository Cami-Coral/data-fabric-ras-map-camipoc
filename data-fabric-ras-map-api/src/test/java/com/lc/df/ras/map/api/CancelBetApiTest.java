package com.lc.df.ras.map.api;

import com.lc.df.ras.betting.request.CancelBetRequestCommand;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.bet.BetService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.PARAM_API_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CancelBetApiTest extends TestBase {

    private final String RAS_CANCEL_BET_REQUEST = API_ROOT.concat("/cancelBet");
    List<String> mandatoryHeaders = Arrays.asList();

    private String validCancelBetReq = null;
    private String validBetResp = null;

    @Before
    public void setup() throws IOException {
        validCancelBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestValid.xml"), "UTF-8");
        validBetResp = IOUtils.toString(this.getClass().getResourceAsStream("/rasBetResponseValid.xml"), "UTF-8");
    }

    @MockBean
    BetService betService;


    @Test
    public void whenUsernameIsNotPresentThenThrowMissingUsernameException() throws Exception {
        String missingUsernameCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestMissingUsername.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingUsernameCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.MISSING_USERNAME + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Username is missing.</resultMessage>"));
    }

    @Test
    public void whenDeviceTypeIsNotPresentThenThrowREquiredFieldException() throws Exception {
        String missingDeviceTypeCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestMissingDeviceType.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingDeviceTypeCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Device type is missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransCodeIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingParentTransCodeCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestMissingPArentTransCode.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingParentTransCodeCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction code or parent transaction date are missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransDateIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingParentTransDateCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestMissingParentTransDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingParentTransDateCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction code or parent transaction date are missing.</resultMessage>"));
    }

    @Test
    public void whenSlipNumberIsInvalidThenThrowInvalidRequestException() throws Exception {
        String invalidSlipNoCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestInvalidSlipNumber.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidSlipNoCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Slip number [AA] has an invalid format</resultMessage>"));
    }

    @Test
    public void whenSlipDateIsInvalidThenThrowInvalidRequestException() throws Exception {
        String invalidSlipDateCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestInvalidSlipDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidSlipDateCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Slip date [2aa] is not a valid format</resultMessage>"));
    }

    @Test
    public void whenSlipAmountIsInvalidThenThrowInvalidRequestException() throws Exception {
        String invalidSlipAmountCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestInvalidSlipAmount.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidSlipAmountCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>[Slip[321101008698002]] doesn't have a valid value [550AA]</resultMessage>"));
    }

    @Test
    public void whenValidCancelBetRequestIsSuccessfulThenReturnSuccessResult() throws Exception {
        String validCancelRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasCancelBetRequestValid.xml"), "UTF-8");
        CallResult expectedResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(expectedResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validCancelRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        CallResult actualResponse = xmlMapper.readValue(result.getResponse().getContentAsString(), CallResult.class);

        assertEquals(expectedResponse.getResultMessage(), actualResponse.getResultMessage());
        assertEquals(expectedResponse.getResultCode(), actualResponse.getResultCode());
        assertEquals(expectedResponse.getUsername(), actualResponse.getUsername());
        assertEquals(expectedResponse.getMissingFunds(), actualResponse.getMissingFunds());
    }

    @Test
    public void testMandatoryHeaders() {
        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).cancelBet(any(CancelBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_CANCEL_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validCancelBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(headers);

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        Assert.assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }
}
