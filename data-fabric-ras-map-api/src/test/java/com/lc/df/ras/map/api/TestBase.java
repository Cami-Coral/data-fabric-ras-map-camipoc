package com.lc.df.ras.map.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lc.df.logging.DataFabricLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

public class TestBase {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DataFabricLogger dataFabricLogger;

    @Autowired
    ObjectMapper objectMapper;

    XmlMapper xmlMapper = new XmlMapper();

    protected HttpHeaders buildMandatoryHeaders(List<String> mandatoryHeaders) {
        HttpHeaders headers = new HttpHeaders();
        mandatoryHeaders.stream().forEach(header -> headers.add(header, "TEST_VALUE"));
        return headers;
    }

}
