package com.lc.df.ras.map.api;

import com.lc.df.ras.betting.request.UnsettleBetRequestCommand;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.bet.BetService;
import com.lc.df.service.utils.Constants;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.MESSAGE_SUCCESS;
import static com.lc.df.service.utils.Constants.PARAM_API_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UnsettleBetApiTest extends TestBase {

    private final String RAS_UNSETTLE_BET_REQUEST = API_ROOT.concat("/unsettleBet");
    List<String> mandatoryHeaders = Arrays.asList();

    private String validUnsettleBetReq = null;
    private String validBetResp = null;

    @Before
    public void setup() throws IOException {
        validUnsettleBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestValid.xml"), "UTF-8");
        validBetResp = IOUtils.toString(this.getClass().getResourceAsStream("/rasBetResponseValid.xml"), "UTF-8");
    }

    @MockBean
    BetService betService;


    @Test
    public void whenSlipNumberContainsAlphaNumericValuesThenSuccess() throws Exception {
        String invalidSlipNoSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestInvalidSlipNumber.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidSlipNoSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        //assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes. + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>" + MESSAGE_SUCCESS + "</resultMessage>"));
    }

    @Test
    public void whenSlipNumberIsMissingThenThrowInvalidRequestException() throws Exception {
        String missingSlipNoSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestMissingSlipNumber.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingSlipNoSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Slip number is missing from the request</resultMessage>"));
    }

    @Test
    public void whenDateIsInvalidThenThrowInvalidRequestException() throws Exception {
        String invalidDateUnsettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestInvalidDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidDateUnsettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Date [201w8-07-05 08:05:10.000] is not a valid format</resultMessage>"));
    }

    @Test
    public void whenDateIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingDateUnsettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestMissingDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingDateUnsettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Date field is missing</resultMessage>"));
    }

    @Test
    public void whenDeviceTypeIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingDeviceTypeUnsettleBetRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestMissingDeviceType.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingDeviceTypeUnsettleBetRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Device type is missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransCodeIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingParentTransCodeUnsettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestMissingParentTransCode.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingParentTransCodeUnsettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction code or parent transaction date are missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransDateIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingParentTransDateUnsettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestMissingParentTransDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingParentTransDateUnsettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction code or parent transaction date are missing.</resultMessage>"));
    }

    @Test
    public void whenParentTransDateIsIncorrectFormatThenThrowInvalidRequestException() throws Exception {
        String invalidParentTransDateUnsettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestInvalidParentTransDate.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidParentTransDateUnsettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Parent transaction date [2018-07-0M4 16:31:27.000] is not a valid format</resultMessage>"));
    }

    @Test
    public void whenAmountIsNotPresentThenThrowRequiredFieldException() throws Exception {
        String missingAmountSettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestMissingAmount.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingAmountSettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Amount field is missing</resultMessage>"));
    }

    @Test
    public void whenAmountIsNegativeThenThrowInvalidRequestException() throws Exception {
        String negativeAmountsUnsettleRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestNegativeAmount.xml"), "UTF-8");
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(negativeAmountsUnsettleRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Slip[133813351675001] has a negative value for the amount[-12000].</resultMessage>"));
    }

    @Test
    public void whenValidUnsettleBetRequestIsSuccessfulThenReturnSuccessResult() throws Exception {
        String validUnsettleBetRequest = IOUtils.toString(this.getClass().getResourceAsStream("/rasUnsettleBetRequestValid.xml"), "UTF-8");
        CallResult expectedResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(expectedResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validUnsettleBetRequest)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        CallResult actualResponse = xmlMapper.readValue(result.getResponse().getContentAsString(), CallResult.class);

        assertEquals(expectedResponse.getResultMessage(), actualResponse.getResultMessage());
        assertEquals(expectedResponse.getResultCode(), actualResponse.getResultCode());
        assertEquals(expectedResponse.getUsername(), actualResponse.getUsername());
        assertEquals(expectedResponse.getMissingFunds(), actualResponse.getMissingFunds());
    }

    @Test
    public void testMandatoryHeaders() {
        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).unsettleBet(any(UnsettleBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_UNSETTLE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validUnsettleBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(headers);

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        Assert.assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }
}
