package com.lc.df.ras.map.api;

import com.lc.df.ras.betting.request.PlaceBetRequestCommand;
import com.lc.df.ras.betting.response.BetPlacementResult;
import com.lc.df.ras.betting.response.CallResult;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.bet.BetService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.PARAM_API_KEY;
import static org.custommonkey.xmlunit.XMLUnit.compareXML;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PlaceBetApiTest extends TestBase {

    private final String RAS_PLACE_BET_REQUEST = API_ROOT.concat("/bet");
    List<String> mandatoryHeaders = Arrays.asList();

    private String validBetReq = null;
    private String validBetResp = null;

    @Before
    public void setup() throws IOException {
        validBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasBetRequestValid.xml"), "UTF-8");
        validBetResp = IOUtils.toString(this.getClass().getResourceAsStream("/rasBetResponseValid.xml"), "UTF-8");
    }

    @MockBean
    BetService betService;


    @Test
    public void whenDeviceTypeIsNotPresentThenThrowBadRequestException() throws Exception {
        String missingDeviceTypeBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasMissingDeviceTypeBetRequest.xml"), "UTF-8");
        BetPlacementResult betResponse = xmlMapper.readValue(validBetResp, BetPlacementResult.class);
        doReturn(betResponse).when(betService).placeBet(any(PlaceBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLACE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingDeviceTypeBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Device type is missing.</resultMessage>"));
    }

    @Test
    public void whenShopCodeIsNotPresentThenThrowBadRequestException() throws Exception {
        String missingShopCodeBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasShoppingCodeMissingBetRequest.xml"), "UTF-8");
        BetPlacementResult betResponse = xmlMapper.readValue(validBetResp, BetPlacementResult.class);
        doReturn(betResponse).when(betService).placeBet(any(PlaceBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLACE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(missingShopCodeBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.REQUIRED_FIELD_MISSING + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>Shop code is missing.</resultMessage>"));
    }

    @Test
    public void whenAmountIsNotIntegerThenThrowInvalidRequestException() throws Exception {
        String invalidAmountBetReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasBetRequestInvalidAmount.xml"), "UTF-8");
        BetPlacementResult betResponse = xmlMapper.readValue(validBetResp, BetPlacementResult.class);
        doReturn(betResponse).when(betService).placeBet(any(PlaceBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLACE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(invalidAmountBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("<resultCode>" + DataFabricErrorCodes.INVALID_REQUEST + "</resultCode>"));
        assertTrue(result.getResponse().getContentAsString().contains("<resultMessage>[Slip[133813351675001]] doesn't have a valid value [550n]</resultMessage>"));
    }

    @Test
    public void whenBetRequestIsPopulatedThenReturnSuccessResponse() throws Exception {
        BetPlacementResult betResponse = xmlMapper.readValue(validBetResp, BetPlacementResult.class);
        doReturn(betResponse).when(betService).placeBet(any(PlaceBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLACE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_XML))
                .andExpect(status().isOk())
                .andReturn();

        compareXML(validBetResp, result.getResponse().getContentAsString());
    }

    @Test
    public void testMandatoryHeaders() {
        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        CallResult betResponse = xmlMapper.readValue(validBetResp, CallResult.class);
        doReturn(betResponse).when(betService).placeBet(any(PlaceBetRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLACE_BET_REQUEST)
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .content(validBetReq)
                .param(PARAM_API_KEY, "abc")
                .headers(headers);

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        Assert.assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }
}
