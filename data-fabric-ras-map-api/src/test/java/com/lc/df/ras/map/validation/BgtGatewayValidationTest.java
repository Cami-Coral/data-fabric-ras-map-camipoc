package com.lc.df.ras.map.validation;

import com.lc.df.ras.betting.request.CancelBetRequest;
import com.lc.df.ras.betting.request.Money;
import com.lc.df.ras.betting.request.SettleBetRequest;
import com.lc.df.ras.betting.request.Slip;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.logging.DataFabricLogger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class RasMapValidationTest {

    private final String DEVICE_TYPE = "PC";
    private final String USERNAME = "username";
    private final String PARENT_TRANSACTION_CODE = "321101008698001";
    private final String PARENT_TRANSACTION_DATE = "2018-07-05 08:45:52.000";
    private final String SLIP_NUMBER = "321101008698002";
    private final String SLIP_DATE = "2018-07-05 08:46:40.000";

    @InjectMocks
    private RasMapValidation rasMapValidation;

    @Mock
    protected DataFabricLogger log;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCancelBetRequestHasMissingUserNameThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.setUsername(null);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Username missing, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.MISSING_USERNAME, e.getCode());
        }
    }

    @Test
    public void whenCancelBetRequestHasMissingDEviceTypeThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.setDeviceType(null);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Device type missing, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, e.getCode());
        }
    }

    @Test
    public void whenCancelBetRequestHasMissingParentTransCodeThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.setParentTransactionCode(null);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Parent transaction code missing, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, e.getCode());
        }
    }

    @Test
    public void whenCancelBetRequestHasMissingParentTransDateThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.setParentTransactionDate(null);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Parent transaction date missing, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.REQUIRED_FIELD_MISSING, e.getCode());
        }
    }

    @Test
    public void whenCancelBetRequestHasInvalidParentTransDateThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.setParentTransactionDate("asda");

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Parent transaction date invalid format, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
        }
    }

    @Test
    public void whenCancelBetRequestHasInvalidSlipNumberThenThrowDFE() {
        String invalidSlipNumber = "AA";
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.getSlip().setSlipNumber(invalidSlipNumber);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Invalid slip number, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals("Slip number [" + invalidSlipNumber + "] has an invalid format", e.getDescription());
        }
    }

    @Test
    public void whenCancelBetRequestHasMissingSlipNumberThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.getSlip().setSlipNumber(null);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Missing slip number, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals("Slip number [" + null + "] has an invalid format", e.getDescription());
        }
    }

    @Test
    public void whenCancelBetRequestHasInvalidSlipDateThenThrowDFE() {
        String invalidSlipDate = "AA";
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.getSlip().setDate(invalidSlipDate);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Invalid slip date, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals("Slip date [" + invalidSlipDate + "] is not a valid format", e.getDescription());
        }
    }

    @Test
    public void whenCancelBetRequestHasMissingSlipDateThenThrowDFE() {
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.getSlip().setDate(null);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Missing slip date, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals("Slip date [" + null + "] is not a valid format", e.getDescription());
        }
    }

    @Test
    public void whenCancelBetRequestHasInvalidSlipAmountThrowDFE() {
        String invalidSlipAmount = "123T";
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.getSlip().getAmount().setPence(invalidSlipAmount);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Invalid slip amount, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals(String.format("[Slip[%s]] doesn't have a valid value [%s]", cancelBetRequest.getSlip().getSlipNumber(), invalidSlipAmount)
                    , e.getDescription());
        }
    }

    @Test
    public void whenCancelBetRequestHasNegativeSlipAmountThrowDFE() {
        String invalidSlipAmount = "-123";
        CancelBetRequest cancelBetRequest = validCancelBetRequest();
        cancelBetRequest.getSlip().getAmount().setPence(invalidSlipAmount);

        try {
            rasMapValidation.validateCancelBetRequest(cancelBetRequest);
            fail("Negative slip amount, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals(String.format("Slip[%s] has a negative value for the amount[%s].", cancelBetRequest.getSlip().getSlipNumber(), invalidSlipAmount)
                    , e.getDescription());
        }
    }

    @Test
    public void whenValidCancelBetRequestThenValidationShouldPass() {
        try {
            rasMapValidation.validateCancelBetRequest(validCancelBetRequest());
        } catch (DataFabricException e) {
            fail("Valid request, no error expected");
        }
    }

    @Test
    public void whenAmountsAreZeroThenThrowInvalidRequestEx() {
        SettleBetRequest settleBetRequest = validSettleBetRequest();
        settleBetRequest.setWinAmount(Money.builder()
                .pence("0")
                .build());

        try {
            rasMapValidation.validateSettleBetRequest(settleBetRequest);
            fail("Zero values for win and void amount, should fail at the above line");
        } catch (DataFabricException e) {
            assertEquals(DataFabricErrorCodes.INVALID_REQUEST, e.getCode());
            assertEquals("At least one value for [winAmount] or [voidAmount] should be grater than 0", e.getDescription());
        }
    }

    @Test
    public void whenAmountsAreZeroThenThrowInvalidRequestEx1() {
        SettleBetRequest settleBetRequest = validSettleBetRequest();
        settleBetRequest.setWinAmount(Money.builder()
                .pence("0")
                .build());
        settleBetRequest.setVoidAmount(Money.builder()
                .pence("70")
                .build());

        try {
            rasMapValidation.validateSettleBetRequest(settleBetRequest);
        } catch (DataFabricException e) {
            fail("Valid request, no error expected");
        }
    }


    private SettleBetRequest validSettleBetRequest() {
        return SettleBetRequest.builder()
                .slipNumber(SLIP_NUMBER)
                .username(USERNAME)
                .winAmount(Money.builder()
                        .pence("70")
                        .build())
                .voidAmount(Money.builder()
                        .pence("0")
                        .build())
                .deviceType(DEVICE_TYPE)
                .date(PARENT_TRANSACTION_DATE)
                .parentTransactionCode(PARENT_TRANSACTION_CODE)
                .parentTransactionDate(PARENT_TRANSACTION_DATE)
                .build();
    }

    private CancelBetRequest validCancelBetRequest() {
        return CancelBetRequest.builder()
                .username(USERNAME)
                .deviceType(DEVICE_TYPE)
                .parentTransactionCode(PARENT_TRANSACTION_CODE)
                .parentTransactionDate(PARENT_TRANSACTION_DATE)
                .slip(Slip.builder()
                        .slipNumber(SLIP_NUMBER)
                        .amount(Money.builder()
                                .pence("122")
                                .build())
                        .date(SLIP_DATE)
                        .description("Description")
                        .promotionCode("PROMO")
                        .build())
                .build();
    }
}
