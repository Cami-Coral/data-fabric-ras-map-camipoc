package com.lc.df.ras.map.api;

import com.lc.df.ras.username.request.UsernameRequestCommand;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.username.UsernameService;
import com.playtech.ims.GetIdTokenInfoResponse;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsernameApiTest extends TestBase {

    private final String RAS_USERNAME_REQUEST = API_ROOT.concat("/imsapis/getusername.php");
    private final String USERNAME = "username";
    private final String MESSAGE_ID_OK = "MSG-OK";
    private final String CASINO = "Casino1";

    List<String> mandatoryHeaders = Arrays.asList();

    @MockBean
    UsernameService usernameService;

    @Test
    public void whenValidCardIsProvidedThenUserDetailsAreReturned() throws Exception {
        GetIdTokenInfoResponse getIdTokenInfoResponse = new GetIdTokenInfoResponse();
        getIdTokenInfoResponse.setUsername(USERNAME);
        getIdTokenInfoResponse.setMessageId(MESSAGE_ID_OK);
        GetIdTokenInfoResponse.AttachedPlayer attachedPlayer = new GetIdTokenInfoResponse.AttachedPlayer();
        attachedPlayer.setCasino(CASINO);
        attachedPlayer.setUsername(USERNAME);
        getIdTokenInfoResponse.setAttachedPlayer(attachedPlayer);

        doReturn(xmlMapper.writeValueAsString(getIdTokenInfoResponse)).when(usernameService).getUsernameByCardNumber(any(UsernameRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(RAS_USERNAME_REQUEST)
                .headers(buildMandatoryHeaders(mandatoryHeaders))
                .param(PARAM_CARD_NUMBER, "123");

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        String xmlResponse = result.getResponse().getContentAsString();

        assertNotNull(xmlResponse);
        assertTrue(xmlResponse.contains(USERNAME));
        assertTrue(xmlResponse.contains(MESSAGE_ID_OK));
        assertTrue(xmlResponse.contains(CASINO));
    }

    @Test
    public void whenCardNumberIsBlankProvidedInThePathThenInvalidCardNumberIsThrown() throws Exception {
        GetIdTokenInfoResponse getIdTokenInfoResponse = new GetIdTokenInfoResponse();
        doReturn(xmlMapper.writeValueAsString(getIdTokenInfoResponse)).when(usernameService).getUsernameByCardNumber(any(UsernameRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(RAS_USERNAME_REQUEST)
                .headers(buildMandatoryHeaders(mandatoryHeaders))
                .param(PARAM_CARD_NUMBER, " ");

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains(DataFabricErrorCodes.INVALID_CARD_NUMBER));
    }

    @Test
    public void whenCardNumberIsNotProvidedInThePathThenExceptionIsThrown() throws Exception {
        GetIdTokenInfoResponse getIdTokenInfoResponse = new GetIdTokenInfoResponse();
        doReturn(xmlMapper.writeValueAsString(getIdTokenInfoResponse)).when(usernameService).getUsernameByCardNumber(any(UsernameRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(RAS_USERNAME_REQUEST)
                .headers(buildMandatoryHeaders(mandatoryHeaders));
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("MISSING-REQUIRED-PARAMETERS"));
    }

    @Test
    public void testMandatoryHeaders() {
        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        GetIdTokenInfoResponse getIdTokenInfoResponse = new GetIdTokenInfoResponse();
        doReturn(xmlMapper.writeValueAsString(getIdTokenInfoResponse)).when(usernameService).getUsernameByCardNumber(any(UsernameRequestCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(RAS_USERNAME_REQUEST)
                .headers(headers)
                .param(PARAM_CARD_NUMBER, "123");

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }

}
