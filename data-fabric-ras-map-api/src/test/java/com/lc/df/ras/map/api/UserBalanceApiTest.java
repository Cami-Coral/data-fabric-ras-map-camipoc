package com.lc.df.ras.map.api;

import com.lc.df.ras.balance.request.UserBalanceCommand;
import com.lc.df.ras.balance.response.BalanceResponse;
import com.lc.df.error.service.config.DataFabricErrorCodes;
import com.lc.df.service.balance.BalanceService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static com.lc.df.service.utils.Constants.API_ROOT;
import static com.lc.df.service.utils.Constants.PARAM_API_KEY;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserBalanceApiTest extends TestBase {

    private final String RAS_PLAYER_BALANCE_REQUEST = API_ROOT.concat("/getPlayerBalances");
    List<String> mandatoryHeaders = Arrays.asList();

    @MockBean
    BalanceService balanceService;

    @Test
    public void whenCardNumberProvidedThenPlayersBalanceShouldBeReturned() throws Exception {
        String rasBalanceReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasPlayerBalanceRequest.json"), "UTF-8");
        String rasBalanceResp = IOUtils.toString(this.getClass().getResourceAsStream("/rasPlayerBalanceResponse.json"), "UTF-8");
        BalanceResponse balanceResponse = objectMapper.readValue(rasBalanceResp, BalanceResponse.class);

        doReturn(balanceResponse).when(balanceService).getPlayerBalanceByCardNumber(any(UserBalanceCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLAYER_BALANCE_REQUEST)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rasBalanceReq)
                .param(PARAM_API_KEY, "abc")
                .headers(buildMandatoryHeaders(mandatoryHeaders));

        MvcResult result = mockMvc.perform(requestBuilder).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JSONAssert.assertEquals(rasBalanceResp, result.getResponse().getContentAsString(), false);
    }


    @Test
    public void testMandatoryHeaders() {

        mandatoryHeaders.forEach(header -> {
            HttpHeaders headers = buildMandatoryHeaders(mandatoryHeaders);
            headers.remove(header);
            try {
                checkHeaderMissing(headers, header);
            } catch (Exception e) {
                Assert.fail("Unexpected error");
            }
        });
    }

    private void checkHeaderMissing(HttpHeaders headers, String missingHeader) throws Exception {
        String rasBalanceReq = IOUtils.toString(this.getClass().getResourceAsStream("/rasPlayerBalanceRequest.json"), "UTF-8");
        String rasBalanceResp = IOUtils.toString(this.getClass().getResourceAsStream("/rasPlayerBalanceResponse.json"), "UTF-8");
        BalanceResponse balanceResponse = objectMapper.readValue(rasBalanceResp, BalanceResponse.class);

        doReturn(balanceResponse).when(balanceService).getPlayerBalanceByCardNumber(any(UserBalanceCommand.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(RAS_PLAYER_BALANCE_REQUEST)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rasBalanceReq)
                .param(PARAM_API_KEY, "abc")
                .headers(headers);

        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

        Assert.assertTrue(StringUtils.containsIgnoreCase(result.getResolvedException().getMessage(), missingHeader));
    }
}
