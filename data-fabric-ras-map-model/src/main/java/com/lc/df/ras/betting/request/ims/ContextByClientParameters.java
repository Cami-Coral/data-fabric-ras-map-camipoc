package com.lc.df.ras.betting.request.ims;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlRootElement(
        name = "ContextByClientParameters",
        namespace = "http://www.playtech.com/services/player-management"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ContextByClientParameters {
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String clientPlatform;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String clientType;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String languageCode;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String venue;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String deviceType;
}
