package com.lc.df.ras.betting.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BetRequestCommand {
    private Headers headers;
    private String locale;
    private String url;
    private String appName;
}
