package com.lc.df.ras;

import lombok.*;

@Builder
@Getter
public class Headers {
    private String xClientRequestId;
    private String xForwardedFor;
    private String xLcCid;
    private String casinoName;
}
