package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UnsettleBetRequest {
    private String slipNumber;
    private String username;
    private Money amount;
    private String date;
    private String deviceType;
    private String parentTransactionCode;
    private String parentTransactionDate;
}
