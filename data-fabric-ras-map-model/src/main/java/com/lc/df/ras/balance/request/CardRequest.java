package com.lc.df.ras.balance.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CardRequest {

    private String cardUID;
    private String cardPIN;
    private String cardNumber;
}
