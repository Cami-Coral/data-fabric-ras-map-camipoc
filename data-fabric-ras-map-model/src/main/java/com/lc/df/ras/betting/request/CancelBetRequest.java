package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CancelBetRequest {
    private Slip slip;
    private String username;
    private String deviceType;
    private String parentTransactionCode;
    private String parentTransactionDate;
}
