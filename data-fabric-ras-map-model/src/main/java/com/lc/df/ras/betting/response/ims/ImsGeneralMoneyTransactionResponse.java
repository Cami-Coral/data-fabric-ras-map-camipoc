package com.lc.df.ras.betting.response.ims;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Getter
@Setter
@XmlRootElement(
        name = "generalMoneyTransactionResponse",
        namespace = "http://www.playtech.com/services/wallet"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ImsGeneralMoneyTransactionResponse {
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String transactionCode;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String transactionCodeInUms;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    protected String messageId;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    protected String errorCode;

    @XmlTransient
    protected String methodName;
}
