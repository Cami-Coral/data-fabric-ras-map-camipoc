package com.lc.df.ras.betting.request.ims;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlRootElement(
        name = "amount",
        namespace = "http://www.playtech.com/services/wallet"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class Amount {
    @XmlElement(
            namespace = "http://www.playtech.com/services/common"
    )
    public String amount;

    @XmlElement(
            namespace = "http://www.playtech.com/services/common"
    )
    public String currencyCode;
}
