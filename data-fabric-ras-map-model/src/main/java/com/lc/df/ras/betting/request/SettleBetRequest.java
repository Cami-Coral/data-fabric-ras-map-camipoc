package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SettleBetRequest {
    private String slipNumber;
    private String username;
    private Money winAmount;
    private Money voidAmount;
    private String date;
    private String deviceType;
    private String description;
    private String parentTransactionCode;
    private String parentTransactionDate;
}
