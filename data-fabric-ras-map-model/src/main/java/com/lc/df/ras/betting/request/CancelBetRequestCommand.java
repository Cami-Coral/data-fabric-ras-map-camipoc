package com.lc.df.ras.betting.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
public class CancelBetRequestCommand extends BetRequestCommand {
    private CancelBetRequest cancelBetRequest;

    @Builder(builderMethodName = "cancelBetRequestCommandBuilder")
    public CancelBetRequestCommand(Headers headers, String locale, String url, String appName, CancelBetRequest cancelBetRequest) {
        super(headers, locale, url, appName);
        this.cancelBetRequest = cancelBetRequest;
    }
}
