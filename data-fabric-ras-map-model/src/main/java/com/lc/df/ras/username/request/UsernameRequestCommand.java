package com.lc.df.ras.username.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UsernameRequestCommand {
    private String appName;
    private Headers headers;
    private String cardNumber;
    private String locale;
    private String url;
}
