package com.lc.df.ras.betting.request.ims;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlRootElement(
        name = "walletBatchRequest",
        namespace = "http://www.playtech.com/services/wallet"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ImsWalletBatchRequest {

    @XmlTransient
    private String username;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    private boolean singleTransaction = true;

    @XmlElement(
            name = "generalMoneyTransactionRequest",
            namespace = "http://www.playtech.com/services/wallet"
    )
    private List<ImsGeneralMoneyTransactionRequest> generalMoneyTransactionRequests;

}
