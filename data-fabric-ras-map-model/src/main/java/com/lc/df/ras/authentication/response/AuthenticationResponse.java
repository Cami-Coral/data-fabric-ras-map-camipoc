package com.lc.df.ras.authentication.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AuthenticationResponse {
    private String resultMessage;
    private int resultCode;
    private String authenticationStatus;
}
