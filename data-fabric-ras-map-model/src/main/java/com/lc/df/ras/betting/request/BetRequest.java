package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class BetRequest {
    private ShopCode shopCode;
    private CardDetails cardLogin;
    private String deviceType;
    private List<Slip> slips;
}
