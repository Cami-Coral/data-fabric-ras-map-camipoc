package com.lc.df.ras.betting.request.ims;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlRootElement(
        name = "parentTransactionReference",
        namespace = "http://www.playtech.com/services/wallet"
)
public class ParentTransactionReference {
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    private String transactionCode;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    private String transactionDate;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    private String actionType;
}
