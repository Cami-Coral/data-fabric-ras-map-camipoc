package com.lc.df.ras.betting.request.ims;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlRootElement(
        name = "generalMoneyTransactionRequest",
        namespace = "http://www.playtech.com/services/wallet"
)
public class ImsGeneralMoneyTransactionRequest {
    @XmlTransient
    public String username;
    @XmlElement(
            namespace = "http://www.playtech.com/services/player-management"
    )
    public ContextByClientParameters contextByClientParameters;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String transactionCode;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String transactionDate;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public Amount amount;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String actionType;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String description;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String clientType;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public String clientPlatform;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public TransactionExtraParameters transactionExtraParameters;
    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    public ParentTransactionReference parentTransactionReference;
}
