package com.lc.df.ras.authentication.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AuthenticationRequestCommand {
    private AuthenticationRequest authenticationRequest;
    private Headers headers;
    private String locale;
    private String url;
    private String appName;
}
