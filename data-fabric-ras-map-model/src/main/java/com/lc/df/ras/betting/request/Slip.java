package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Slip {
    private String slipNumber;
    private String date;
    private String description;
    private String promotionCode;
    private Money amount;
}
