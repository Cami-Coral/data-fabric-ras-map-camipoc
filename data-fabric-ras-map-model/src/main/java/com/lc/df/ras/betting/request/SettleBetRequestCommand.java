package com.lc.df.ras.betting.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
public class SettleBetRequestCommand extends BetRequestCommand {
    private SettleBetRequest settleBetRequest;

    @Builder(builderMethodName = "settleBetRequestCommandBuilder")
    public SettleBetRequestCommand(Headers headers, String locale, String url, String appName, SettleBetRequest settleBetRequest) {
        super(headers, locale, url, appName);
        this.settleBetRequest = settleBetRequest;
    }
}
