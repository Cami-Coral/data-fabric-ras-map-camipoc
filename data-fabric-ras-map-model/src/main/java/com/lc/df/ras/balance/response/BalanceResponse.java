package com.lc.df.ras.balance.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BalanceResponse {
    private String resultMessage;
    private String resultCode;
    private String totalAccountBalance;
    private String otcBettingBalance;
    private String otcWithdrawableBalance;
}
