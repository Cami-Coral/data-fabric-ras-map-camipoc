package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Money {
    private String pence;
}
