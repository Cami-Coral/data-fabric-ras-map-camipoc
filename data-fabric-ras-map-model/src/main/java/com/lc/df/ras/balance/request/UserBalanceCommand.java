package com.lc.df.ras.balance.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserBalanceCommand {
    private CardRequest cardRequest;
    private Headers headers;
    private String locale;
    private String imsTokenInfoUrl;
    private String url;
    private String appName;
}
