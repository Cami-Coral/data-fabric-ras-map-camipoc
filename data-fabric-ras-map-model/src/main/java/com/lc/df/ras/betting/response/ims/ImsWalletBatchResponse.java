package com.lc.df.ras.betting.response.ims;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@XmlRootElement(
        name = "walletBatchResponse",
        namespace = "http://www.playtech.com/services/wallet"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ImsWalletBatchResponse {

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    protected String messageId;

    @XmlElement(
            namespace = "http://www.playtech.com/services/wallet"
    )
    protected String errorCode;

    @XmlTransient
    protected String methodName;

    @XmlElement(
            name = "generalMoneyTransactionResponse",
            namespace = "http://www.playtech.com/services/wallet"
    )
    public List<ImsGeneralMoneyTransactionResponse> generalMoneyTransactionResponses;
}
