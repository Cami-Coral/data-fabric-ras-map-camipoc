package com.lc.df.ras.betting.response;

import com.lc.df.ras.betting.request.Money;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BetPlacementResult extends CallResult {

    @Builder(builderMethodName = "betPlacementResultBuilder")
    public BetPlacementResult(String resultMessage, String resultCode, String username, Money missingFunds) {
        super(resultMessage, resultCode, username, missingFunds);
    }

}
