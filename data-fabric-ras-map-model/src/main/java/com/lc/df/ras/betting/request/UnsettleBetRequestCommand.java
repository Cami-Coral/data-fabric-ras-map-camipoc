package com.lc.df.ras.betting.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
public class UnsettleBetRequestCommand extends BetRequestCommand {
    private UnsettleBetRequest unsettleBetRequest;

    @Builder(builderMethodName = "unsettleBetRequestCommandBuilder")
    public UnsettleBetRequestCommand(Headers headers, String locale, String url, String appName, UnsettleBetRequest unsettleBetRequest) {
        super(headers, locale, url, appName);
        this.unsettleBetRequest = unsettleBetRequest;
    }
}
