package com.lc.df.ras.authentication.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CardLogin {
    private String cardNumber;
    private String cardPIN;
}
