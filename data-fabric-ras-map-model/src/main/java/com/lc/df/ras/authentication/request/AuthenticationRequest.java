package com.lc.df.ras.authentication.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AuthenticationRequest {
    private String deviceType;
    private String shopCode;
    private CardLogin cardLogin;
}
