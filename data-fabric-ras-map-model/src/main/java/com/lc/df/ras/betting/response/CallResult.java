package com.lc.df.ras.betting.response;

import com.lc.df.ras.betting.request.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallResult {
    private String resultMessage;
    private String resultCode;
    private String username;
    private Money missingFunds;
}
