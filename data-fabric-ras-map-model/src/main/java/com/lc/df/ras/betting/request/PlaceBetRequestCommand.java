package com.lc.df.ras.betting.request;

import com.lc.df.ras.Headers;
import lombok.Builder;
import lombok.Getter;

@Getter
public class PlaceBetRequestCommand extends BetRequestCommand {
    private BetRequest betRequest;
    private String urlImsUsername;
    private String urlImsUserBalance;

    @Builder(builderMethodName = "placeBetRequestCommandBuilder")
    public PlaceBetRequestCommand(Headers headers, String locale, String url, String appName, BetRequest betRequest, String urlImsUsername, String urlImsUserBalance) {
        super(headers, locale, url, appName);
        this.betRequest = betRequest;
        this.urlImsUsername = urlImsUsername;
        this.urlImsUserBalance = urlImsUserBalance;
    }
}
