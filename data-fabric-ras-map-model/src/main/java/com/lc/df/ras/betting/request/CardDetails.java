package com.lc.df.ras.betting.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CardDetails {
    private String cardUID;
    private String cardPIN;
    private String cardNumber;
}
