#! /bin/sh
# build.sh
# Copyright (C) 2017 Mateusz Pawlowski <mateusz@generik.co.uk>
# Distributed under terms of the MIT license.


mvn clean package -X -U  -s ci/settings.xml


