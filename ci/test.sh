#! /bin/sh
#
# test.sh
# Copyright (C) 2017 Mateusz Pawlowski <mateusz@generik.co.uk>
#
# Distributed under terms of the MIT license.
#

mvn sonar:sonar \
  -Dsonar.host.url=https://sonar.datafabric.infra.aws.ladbrokescoral.com \
  -Dsonar.login=${SONARTOKEN}


echo "tested in build step"
